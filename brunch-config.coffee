exports.config =
  # See http://brunch.io/#documentation for docs.
  files:
    javascripts:
      joinTo: 'app.js'
    stylesheets:
      joinTo: 'app.css'
    templates:
      joinTo: 'app.js'

      

plugins:
    brunchTypescript:
        ignoreErrors: false
        noEmit: false
        removeComments: true
        watch: false
    babel:
        presets: ['es2015', 'es2016', 'es2017']


    

    
    
