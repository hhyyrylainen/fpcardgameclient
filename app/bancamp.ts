import { Pos2, Rect } from "./types";
import config from "./config";
import { Renderable, drawOutline } from "./drawable";
import { Card } from "./card";

export class BanCamp extends Renderable{

    cards: Array<Card> = [];

    /**
       The number of rows all the cards would take up
       Will be useful for paging widget
    */
    maxRows: number = 0;

    /**
       Selected scroll offset. Must be less than maxRows
    */
    startRow: number = 0;

    constructor(){

        super(new Rect(0.708, 0, 0.290, 0.85));
    }

    addCard(card: Card){

        // Mark as banned //
        card.banned = true;
        
        this.cards.push(card);
        this.repositionCards();
    }

    removeCard(card: Card){

        let index = this.cards.indexOf(card);

        if(index < 0)
            throw "camp removeCard didn't find the card";

        card.occluded = false;
        card.banned = false;
        this.cards.splice(index, 1);
        this.repositionCards();
    }

    repositionCards(){

        let posx = 5;
        let posy = 53;

        let currentrow = 0;

        for(let element of this.cards){
            
            if(currentrow < this.startRow ||
               (posy + config.fieldCardHeight + 3 > this.pixelRect.height))
            {

                element.occluded = true;
                
            } else {

                element.occluded = false;
                let pos = this.relativeToAbsolute(new Pos2(posx, posy));

                element.setHome(pos);
                element.moveHome(900, true);
            }

            posx += config.fieldCardWidth + config.BANCAMP_PADDING;

            if(posx + config.fieldCardWidth >= this.pixelRect.width){

                // Next row //
                ++currentrow;
                
                if(currentrow < this.startRow){

                    // Still skipping rows //
                    
                } else {

                    posy += config.fieldCardHeight + 5;
                }
                
                posx = 5;
            }
        }

        this.maxRows = currentrow;
    }

    draw(ctx: CanvasRenderingContext2D, delta: number): boolean{

        ctx.fillStyle = config.gray;
        ctx.fillRect(this.pixelRect.x, this.pixelRect.y,
                     this.pixelRect.width, this.pixelRect.height);

        // Text //
        ctx.fillStyle = config.FpRed;
        ctx.textAlign = "center";
        ctx.font = "32px serif";

        // Shadow
        ctx.fillStyle = "black";
        ctx.fillText("Refugee Camp", this.pixelRect.x+2 + (this.pixelRect.width / 2),
                     this.pixelRect.y + 25 + 2);
        
        ctx.fillStyle = config.FpRed;
        ctx.fillText("Refugee Camp", this.pixelRect.x + (this.pixelRect.width / 2),
                     this.pixelRect.y + 25);

        // Border //
        drawOutline(ctx, this.getRect(), "red", false);

        return false;
    }

    onResolutionUpdated(){

        super.onResolutionUpdated();

        // This can be called before the cards have been created so this check is needed
        if(this.cards)
            this.repositionCards();
    }    
}

