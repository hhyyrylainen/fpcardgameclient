import * as $ from 'jquery';

import config from "./config";
import { Card, cardholder } from "./card";
import { game } from "./gamemain";
import { PlayCardAction, AttackAction } from "./actions";

/**
   Message type enumeration
   Needs to match the one in server/source/messages.d
*/
export enum MessageType{
    // Server kick player because of an error
    Error = 1,

    // First message. logs in the player. Server responds with the same message confirming the name
    Hello = 120,
    
    // Client wants to play against someone
    StartRequest = 121,
    
    // Server accepts a match and the client will start receiving match events. Like DrawHand
    // and CardAction and TurnChange
    MatchStart = 122,
    
    // Once a match ends by win condition or forfeit or disconnect
    MatchEnd = 123,
    
    // Client or his opponent draws cards
    DrawHand = 124,

    // Client ends his turn
    TurnEndRequest = 125,

    // Server notifies client about changed turn
    TurnChange = 126,

    // Card does something, moves from hand to the playing field, attacks, etc.
    // unused
    CardAction = 127,

    // Server sends a general chat message, nothing too important
    ChatText = 128,

    // Server tells the client the state of a match, if the state == ENDED the player should
    // move to the lobby
    MatchStatusUpdate = 129,

    // Send as a response to a client sent action, tells the client whether the action succeeded
    // or not
    ActionFinished = 130,

    // A card is revealed and the client can now see it
    CardReveal = 131,

    // A card is banned (killed) or unbanned
    CardBannedStatus = 132,

    // A card property (attack, defence points) has updated
    CardStatsUpdated = 133,

    // Server tells a client the currently online players
    ClientList = 134,

    // Server notifies a client about a new client or a disconnected client
    ClientStatus = 135,

    // Contains a single chat message either from the server to the client or
    // from the server broadcast to clients
    ChatMessage = 136,

    // Client challenges another to a match
    // Or cancels that challenge
    // Also used to accept the challenge if players challenge each other at the same time
    ClientChallengeOther = 137,

    // Server replies about whether the challenge failed or the other accepted it
    ChallengeStatus = 138,
    
    // Client is notified that it has been challenged to a match
    NotifyChallenged = 139,

    // Client needs to popup a message saying the serve ris going down
    ServerGoingDown = 140,

    // Client is told that the game has ended and should go back to main screen
    ClientExitMatch = 141,

    // Client wants to give up and lose a match
    ClientRequestDefeat = 142,

    // Energy of a player has updated
    EnergyUpdated = 143,

    // Server has selected a thread for the current match
    ThreadSelect = 144,

    // Server has given the player the option to make a post
    PostDrawn = 145,

    // Server informs a player that their possible post is no longer available
    PostDecayed = 146,

    // Winner ratings of a player has updated
    WinnerRatingsUpdated = 147,

    // When client needs to choose a target for a spell / post
    TargetRequired = 148,

    // A new post has been made
    NewPostDone = 149,

    // A player is now accepting or denying challenges
    ClientAllowChallenge = 150,

    // A card has been completely destroyed and should be removed from the game
    CardVoided = 151,

    // The client wants to discard an effect, if not allowed by the server the client
    // will be prompted for a new target
    DiscardCardEffect = 152,

    // Server tells the client current settings
    GameRuleSettings = 153,

    // Server has just applied a spell effect and the client should show an effect
    PlaySpellEffect = 154,
    
    // All of these actions work like this: client creates them,
    // applies them locally and then sends to the server for
    // validation. If the server rejects the action the client will
    // undo it. The client will also receive actions for their
    // opponen't from the server, these will never be undone

    // Playing a new card to the battlefield
    ActionPlayCard = 500,

    // Player attacks a target with a card
    ActionAttack = 501,

    // Player posts a post that the server has told the client about in a PostDrawn message
    ActionPost = 502,

    // Client has targeted something
    ActionTargetSelected = 503,
}

// Has all the messages the client wants to receive
let idToClassMapper: { [id: number] : (json: any) => BaseMessage; } = {}


/**
   Creates a message when received
*/
export function constructMessageFromJson(jsonBlob: any): BaseMessage{

    if(!("id" in jsonBlob)){

        throw "message blob is missing id";
    }

    let id = jsonBlob["id"];

    if(!(id in idToClassMapper)){

        throw "Unknown message type received: " + id;        
    }

    return idToClassMapper[id](jsonBlob);
}


/**
   Base type for all messages
*/
export abstract class BaseMessage{

    constructor(public id: number){

    }

    /**
       Assigns all properties from json
    */
    copyProperties(json: any, propList: Array<string>){

        if(this.id != json["id"])
            throw "Trying to copy message properties from a json blob with different id";

        // Assign properties
        for(let prop of propList){

            (this as any)[prop] = json[prop];
        }

        // Asigning undefined from json to already defined things in this will erase
        // the property from this, so this verify works as intended
        
        // Verify all are now correct
        this.verifyProperties(propList);
    }
    
    /**
       Verifies all properties exist on this object
    */
    verifyProperties(propList: Array<string>){

        for(let prop of propList){
            
            if(!(prop in this) || (this as any)[prop] === undefined){

                let error = "Message object is missing property: " + prop;
  		        console.log(error);
                throw error;
            }
        }
    }

    /**
       Lets the game know that this was received, this contains all the logic for handling
       the message. Will throw if didn't work
    */
    abstract handle(): void;
}


//
// Message classes
//

export class HelloMessage extends BaseMessage{

    version: string = config.version;
    
    constructor(public name: string){

        super(MessageType.Hello);
    }

    static receive(json: any): HelloMessage{

        let created = new HelloMessage(undefined);

        created.copyProperties(json, ["version", "name"]);
        return created;
    }

    handle(){

        game.networking.waitingPostInit = false;
        
        // Good to go //
        console.log("Confirmed name: " + this.name);
        
        game.updateName(this.name);
        game.postInit();
    }
}


export class ErrorMessage extends BaseMessage{

    constructor(public error: string){
        
        super(MessageType.Error);
    }

    static receive(json: any): ErrorMessage{

        let created = new ErrorMessage(undefined);

        created.copyProperties(json, ["error"]);
        return created;
    }

    handle(){

        game.doErrorRedirect("Server sent error: " + this.error);
    }
}

/**
   Used to start a match. Will always receive MatchStart start or MatchEnd from the server
*/
export class StartRequestMessage extends BaseMessage{

    constructor(public opponent: string, public deck: string = "default"){
        
        super(MessageType.StartRequest);
    }
    
    static receive(json: any): StartRequestMessage{

        let created = new StartRequestMessage(undefined, undefined);

        created.copyProperties(json, ["opponent", "deck"]);
        return created;
    }

    handle(){
        throw "client tried to handle message that is send only";
    }
}

/**
   Used to end current player's turn, does nothing if not currently this player's turn
*/
export class EndTurnRequestMessage extends BaseMessage{

    constructor(){
        
        super(MessageType.TurnEndRequest);
    }

    static receive(json: any): EndTurnRequestMessage{

        let created = new EndTurnRequestMessage();

        created.copyProperties(json, []);
        return created;
    }

    handle(){

        throw "client tried to handle message that is send only";
    }
}

export class DiscardCardEffectMessage extends BaseMessage{

    constructor(){
        
        super(MessageType.DiscardCardEffect);
    }

    static receive(json: any): DiscardCardEffectMessage{
        
        let created = new DiscardCardEffectMessage();
        
        created.copyProperties(json, []);
        return created;
    }
    
    handle(){

        throw "client tried to handle message that is send only";
    }
}

// Challenging another player works like this:
// first send this message with opponents name and display the wait dialog
// If the player cancels send another this type of message with false as the stillvalid
// value
// If the opponent accepts we will receive a ChallengeStatus set to succeeded
// at this point we switch to the waiting screen when we will receive a match confirmed message
// If the challenge status is false we report that the challenge failed
//
// Meanwhile the opponent will see this:
// They receive NotifyChallenged if they aren't in game or (possibly even if they are in an AI
// match)
// Then they send a message of this type with the stillvalid indicating whether they accepted it
// or not. After that they receive a ChallengeStatus which indicates whethey the challenge was
// not valid anymore or that it succeeded and the player should switch to the waiting screen
// to wait for a MatchStart message
export class ClientChallengeRequest extends BaseMessage{

    constructor(public status: boolean, public opponent: string){
        
        super(MessageType.ClientChallengeOther);
    }

    static receive(json: any): ClientChallengeRequest{

        let created = new ClientChallengeRequest(undefined, undefined);

        created.copyProperties(json, ["status", "opponent"]);
        return created;
    }

    handle(){

        throw "todo: do";
    }
}

/**
   Send when the player wants to give up
*/
export class ClientRequestDefeatMessage extends BaseMessage{

    constructor(){
        
        super(MessageType.ClientRequestDefeat);
    }

    static receive(json: any): ClientRequestDefeatMessage{
        
        let created = new ClientRequestDefeatMessage();
        
        created.copyProperties(json, []);
        return created;
    }
    
    handle(){
        
        throw "client tried to handle message that is send only";
    }
}

export class MatchStartMessage extends BaseMessage{

    constructor(public opponent: string){
        
        super(MessageType.MatchStart);
    }

    static receive(json: any): MatchStartMessage{

        let created = new MatchStartMessage(undefined);

        created.copyProperties(json, ["opponent"]);
        return created;
    }

    handle(){
        
        // Start match //
        console.log("Our opponent has showed up");
        
        game.onConfirmMatch(this.opponent);
    }
}

export class DrawHandMessage extends BaseMessage{

    constructor(public cards: any[], public count: number, public opponents: boolean){
        
        super(MessageType.DrawHand);
    }

    static receive(json: any): DrawHandMessage{

        let created = new DrawHandMessage(undefined, undefined, undefined);

        created.copyProperties(json, ["cards", "count", "opponents"]);
        return created;
    }

    handle(){
        
        // We or our opponent drew card(s) //
        console.log("Draw cards");

        let opponents = this.opponents;
        
        // We can safely ignore count and just loop through the cards array //
        for(let element of this.cards){
            
            game.onReceiveDrawnCard(cardholder.receiveCard(element), opponents);
        };
    }
}

export class TurnChangeMessage extends BaseMessage{

    constructor(public ownturn: boolean){
        
        super(MessageType.TurnChange);
    }

    static receive(json: any): TurnChangeMessage{

        let created = new TurnChangeMessage(undefined);

        created.copyProperties(json, ["ownturn"]);
        return created;
    }

    handle(){
        
        console.log('turn changed');
        
        if(this.ownturn){

            game.notifyTurn(true);
            
        } else {

            game.notifyTurn(false);
        }
    }
}

export class CardBannedStatusMessage extends BaseMessage{

    constructor(public matchid: number, public status: boolean, public rescuer: boolean){
        
        super(MessageType.CardBannedStatus);
    }

    static receive(json: any): CardBannedStatusMessage{

        let created = new CardBannedStatusMessage(undefined, undefined, undefined);

        created.copyProperties(json, ["matchid", "status", "rescuer"]);
        return created;
    }

    handle(){
        
        game.notifyCardBanned(this.matchid,
                              this.status, this.rescuer);
    }
}

export class GameRuleSettingsMessage extends BaseMessage{

    constructor(public maxwinnerratings: number){
        
        super(MessageType.GameRuleSettings);
    }

    static receive(json: any): GameRuleSettingsMessage{

        let created = new GameRuleSettingsMessage(undefined);

        created.copyProperties(json, ["maxwinnerratings"]);
        return created;
    }

    handle(){
        
        game.notifyGameRuleSettings(this);
    }
}

export class CardVoidedMessage extends BaseMessage{

    constructor(public matchid: number){
        
        super(MessageType.CardVoided);
    }

    static receive(json: any): CardVoidedMessage{

        let created = new CardVoidedMessage(undefined);

        created.copyProperties(json, ["matchid"]);
        return created;
    }

    handle(){

        game.notifyCardVoided(this.matchid);
    }
}

export class CardStatsUpdatedMessage extends BaseMessage{

    constructor(public matchid: number, public attack: number, public defence: number){
        
        super(MessageType.CardStatsUpdated);
    }

    static receive(json: any): CardStatsUpdatedMessage{

        let created = new CardStatsUpdatedMessage(undefined, undefined, undefined);

        created.copyProperties(json, ["matchid", "attack", "defence"]);
        return created;
    }

    handle(){

        game.notifyCardStats(this.matchid,
                             this.attack, this.defence);
    }
}

export class PlaySpellEffectMessage extends BaseMessage{

    constructor(public matchcard: number, public effectname: string, public origin: string){
        
        super(MessageType.PlaySpellEffect);
    }

    static receive(json: any): PlaySpellEffectMessage{

        let created = new PlaySpellEffectMessage(undefined, undefined, undefined);

        created.copyProperties(json, ["matchcard", "effectname", "origin"]);
        return created;
    }

    handle(){

        game.notifyPlaySpellEffect(this.effectname, this.matchcard,
                                   this.origin);
    }
}

export class CardRevealMessage extends BaseMessage{

    constructor(public card: any){
        
        super(MessageType.CardReveal);
    }

    static receive(json: any): CardRevealMessage{

        let created = new CardRevealMessage(undefined);

        created.copyProperties(json, ["card"]);
        return created;
    }

    handle(){

        game.rules.onRevealedCard(this.card);
    }
}

export class ClientListMessage extends BaseMessage{

    constructor(public players: any[]){
        
        super(MessageType.ClientList);
    }

    static receive(json: any): ClientListMessage{

        let created = new ClientListMessage(undefined);

        created.copyProperties(json, ["players"]);
        return created;
    }

    handle(){

        game.notifyOnlinePlayers(this.players);
    }
}

export class ClientAllowChallengeMessage extends BaseMessage{

    constructor(public player: string, public challenge: boolean){
        
        super(MessageType.ClientAllowChallenge);
    }

    static receive(json: any): ClientAllowChallengeMessage{

        let created = new ClientAllowChallengeMessage(undefined, undefined);

        created.copyProperties(json, ["player", "challenge"]);
        return created;
    }

    handle(){

        game.notifyAllowChallenge(this.player, this.challenge);
    }
}

export class ClientStatusMessage extends BaseMessage{

    constructor(public player: string, public online: boolean){
        
        super(MessageType.ClientStatus);
    }

    static receive(json: any): ClientStatusMessage{

        let created = new ClientStatusMessage(undefined, undefined);

        created.copyProperties(json, ["player", "online"]);
        return created;
    }

    handle(){

        game.notifyOnlinePlayer(this.player, this.online);
    }
}

export class ChallengeStatusMessage extends BaseMessage{

    constructor(public opponent: string, public status: boolean, public message: string){
        
        super(MessageType.ChallengeStatus);
    }

    static receive(json: any): ChallengeStatusMessage{

        let created = new ChallengeStatusMessage(undefined, undefined, undefined);

        created.copyProperties(json, ["opponent", "status", "message"]);
        return created;
    }

    handle(){

        if(this.status == false){

            // Didn't get accepted / failed
            game.matchRequestFailed(this.message);
            
        } else {

            // Move to match //
            console.log("Our challenge has been accepted");
            game.challengeRequestAccepted(this.opponent);
       }
    }
}

export class NotifyChallengedMessage extends BaseMessage{

    constructor(public challenger: string, public valid: boolean){
        
        super(MessageType.NotifyChallenged);
    }

    static receive(json: any): NotifyChallengedMessage{

        let created = new NotifyChallengedMessage(undefined, undefined);

        created.copyProperties(json, ["challenger", "valid"]);
        return created;
    }

    handle(){

        if(this.valid == false){

            // Canceled //
            console.log("Challenge by " + this.challenger +
                        " has been cancelled");
            game.onOtherCancelledMatchRequest();
            
        } else {
            
            console.log("We have been challenged to a match by: " +
                        this.challenger);
            game.receiveMatchRequest(this.challenger);
        }

    }
}

export class ClientExitMatchMessage extends BaseMessage{

    constructor(){
        
        super(MessageType.ClientExitMatch);
    }

    static receive(json: any): ClientExitMatchMessage{

        let created = new ClientExitMatchMessage();

        created.copyProperties(json, []);
        return created;
    }

    handle(){

        console.log("Match ended, moving to main");
        game.notifyExitMatch();
    }
}

export class MatchEndMessage extends BaseMessage{

    constructor(public result: string, public won: boolean, public score: number){
        
        super(MessageType.MatchEnd);
    }

    static receive(json: any): MatchEndMessage{

        let created = new MatchEndMessage(undefined, undefined, undefined);

        created.copyProperties(json, ["result", "won", "score"]);
        return created;
    }

    handle(){

        console.log("Showing match scores");
        game.notifyMatchWon(this.result, this.won, this.score);
    }
}

export class EnergyUpdatedMessage extends BaseMessage{

    constructor(public own: boolean, public energy: number){
        
        super(MessageType.EnergyUpdated);
    }

    static receive(json: any): EnergyUpdatedMessage{

        let created = new EnergyUpdatedMessage(undefined, undefined);

        created.copyProperties(json, ["own", "energy"]);
        return created;
    }

    handle(){

        game.notifyEnergy(this.own, this.energy);
    }
}

export class WinnerRatingsUpdatedMessage extends BaseMessage{

    constructor(public own: boolean, public winner: number){
        
        super(MessageType.WinnerRatingsUpdated);
    }

    static receive(json: any): WinnerRatingsUpdatedMessage{

        let created = new WinnerRatingsUpdatedMessage(undefined, undefined);

        created.copyProperties(json, ["own", "winner"]);
        return created;
    }

    handle(){

        game.notifyWinnerRatings(this.own, this.winner);
    }
}

export class ThreadSelectMessage extends BaseMessage{

    constructor(public title: string, public link: string){
        
        super(MessageType.ThreadSelect);
    }

    static receive(json: any): ThreadSelectMessage{

        let created = new ThreadSelectMessage(undefined, undefined);

        created.copyProperties(json, ["title", "link"]);
        return created;
    }

    handle(){

        game.notifyThread(this.title, this.link);
    }
}


export class PostDrawnMessage extends BaseMessage{

    constructor(public message: string, public effect: string,
                public matchid: number, public decay: number)
    {
        
        super(MessageType.PostDrawn);
    }

    static receive(json: any): PostDrawnMessage{

        let created = new PostDrawnMessage(undefined, undefined, undefined, undefined);

        created.copyProperties(json, ["message", "effect", "matchid", "decay"]);
        return created;
    }

    handle(){

        game.notifyCanDoPost(this.message, this.effect,
                             this.matchid, this.decay);
    }
}

export class NewPostDoneMessage extends BaseMessage{

    constructor(public message: string, public effect: string,
                public poster: string, public link: string)
    {
        
        super(MessageType.NewPostDone);
    }

    static receive(json: any): NewPostDoneMessage{

        let created = new NewPostDoneMessage(undefined, undefined, undefined, undefined);

        created.copyProperties(json, ["message", "effect", "poster", "link"]);
        return created;
    }

    handle(){

        game.notifyNewPost(this.message, this.effect,
                           this.poster, this.link);
    }
}

export class PostDecayedMessage extends BaseMessage{

    constructor(public matchid: number){
        
        super(MessageType.PostDecayed);
    }

    static receive(json: any): PostDecayedMessage{

        let created = new PostDecayedMessage(undefined);

        created.copyProperties(json, ["matchid"]);
        return created;
    }

    handle(){

        game.notifyPostDecayed(this.matchid);
    }
}

export class TargetRequiredMessage extends BaseMessage{

    constructor(public message: string, public canceled: boolean,
                public iscard: boolean, public canbeown: boolean,
                public canbeopponents: boolean)
    {
        
        super(MessageType.TargetRequired);
    }

    static receive(json: any): TargetRequiredMessage{

        let created = new TargetRequiredMessage(undefined, undefined, undefined, undefined,
                                                undefined);

        created.copyProperties(json, ["message", "canceled", "iscard", "canbeown",
                                      "canbeopponents"]);
        return created;
    }

    handle(){

        if(this.canceled){

            game.endSelectTargetMode();

        } else {
            
            game.startSelectTargetMode(this.message,
                                       this.iscard,
                                       this.canbeown,
                                       this.canbeopponents);
        }
    }
}


export class ServerGoingDownMessage extends BaseMessage{

    constructor(public message: string){
        
        super(MessageType.ServerGoingDown);
    }

    static receive(json: any): ServerGoingDownMessage{

        let created = new ServerGoingDownMessage(undefined);

        created.copyProperties(json, ["message"]);
        return created;
    }

    handle(){

        // Let's just popup the dialog here //
        $("#ServerGoingDownMessage").text(this.message);

        $( "#ServerGoingDownDialog" ).dialog({
            resizable: false,
            height: 240,
            width: 480,
            modal: true,
            title: "Server Shutting Down!",
            buttons: {
                close: () => {
                    $( "#ServerGoingDownDialog" ).dialog( "close" );
                }
            }
        }).show();

    }
}

export class ChatMessageMessage extends BaseMessage{

    constructor(public message: string, public sender: string){
        
        super(MessageType.ChatMessage);
    }

    static receive(json: any): ChatMessageMessage{

        let created = new ChatMessageMessage(undefined, undefined);

        created.copyProperties(json, ["message", "sender"]);
        return created;
    }

    handle(){

        if(!this.sender){

            // Server broadcast //
            $("#MessageList").append(
                $('<li>').append($('<p>').append(
                    $('<span>').addClass("ServerMessage").text(
                        this.message))));
            
        } else {

            $("#MessageList").append(
                $('<li>').append($('<p>').append(
                    $('<span>').text(this.sender)).append(
                        $('<span>').addClass("Gray").text(': ')).append(
                            $('<span>').text(this.message))));
        }

        let container = $("#MessageList");
        let scrollTo = container.children().last();

        while(container.children().length > 100){

            // Remove elements //
            container.scrollTop(container.scrollTop() -
                                container.children().first().height());
            container.children().first().remove();
        }
        
        container.animate({
            scrollTop: scrollTo.offset().top - container.offset().top +
                container.scrollTop()
        });

    }
}




//
// Actions
//

export class ActionFinished extends BaseMessage{

    constructor(public actionnumber: number, public succeeded: boolean){
        super(MessageType.ActionFinished);
    }

    static receive(json: any): ActionFinished{

        let created = new ActionFinished(undefined, undefined);

        created.copyProperties(json, ["actionnumber", "succeeded"]);
        return created;
    }

    handle(){

        game.notifyActionFinished(this.actionnumber, this.succeeded);
    } 
}


export class ActionPlayCard extends BaseMessage{

    constructor(public actionnumber: number, public matchcard: number, public cardid: number){
        super(MessageType.ActionPlayCard);
    }

    static receive(json: any): ActionPlayCard{

        let created = new ActionPlayCard(undefined, undefined, undefined);

        created.copyProperties(json, ["actionnumber", "matchcard", "cardid"]);
        return created;
    }

    handle(){

        let action = new PlayCardAction(false, game.rules.getCardByMatchID(this.matchcard));
        action.apply();
    } 
}

export class ActionAttack extends BaseMessage{

    constructor(public actionnumber: number, public attacker: number, public target: number){
        super(MessageType.ActionAttack);

    }

    static receive(json: any): ActionAttack{

        let created = new ActionAttack(undefined, undefined, undefined);

        created.copyProperties(json, ["actionnumber", "attacker", "target"]);
        return created;
    }

    handle(){

        // Damage numbers go through a different way
        let action = new AttackAction(false, game.rules.getCardByMatchID(this.attacker),
                                      game.rules.getCardByMatchID(this.target), 0);
        action.apply();
    } 
}

export class ActionDoPost extends BaseMessage{

    constructor(public actionnumber: number, public matchid: number){
        super(MessageType.ActionPost);
    }

    static receive(json: any): ActionDoPost{

        let created = new ActionDoPost(undefined, undefined);

        created.copyProperties(json, ["actionnumber", "matchid"]);
        return created;
    }

    handle(){

        throw "client tried to handle message that is send only";
    } 
}

export class ActionSelectTarget extends BaseMessage{

    constructor(public actionnumber: number, public targettype: string,
                public targetid: number)
    {
        super(MessageType.ActionTargetSelected);
    }

    static receive(json: any): ActionSelectTarget{

        let created = new ActionSelectTarget(undefined, undefined, undefined);

        created.copyProperties(json, ["actionnumber", "targettype", "targetid"]);
        return created;
    }

    handle(){

        throw "client tried to handle message that is send only";
    }
}


// All the messages that can be received //
idToClassMapper[MessageType.Hello] = HelloMessage.receive;

idToClassMapper[MessageType.Error] = ErrorMessage.receive;

idToClassMapper[MessageType.MatchStart] = MatchStartMessage.receive;
idToClassMapper[MessageType.DrawHand] = DrawHandMessage.receive;
idToClassMapper[MessageType.TurnChange] = TurnChangeMessage.receive;
idToClassMapper[MessageType.CardBannedStatus] = CardBannedStatusMessage.receive;
idToClassMapper[MessageType.GameRuleSettings] = GameRuleSettingsMessage.receive;
idToClassMapper[MessageType.CardVoided] = CardVoidedMessage.receive;
idToClassMapper[MessageType.CardStatsUpdated] = CardStatsUpdatedMessage.receive;
idToClassMapper[MessageType.PlaySpellEffect] = PlaySpellEffectMessage.receive;
idToClassMapper[MessageType.CardReveal] = CardRevealMessage.receive;
idToClassMapper[MessageType.ClientList] = ClientListMessage.receive;
idToClassMapper[MessageType.ClientAllowChallenge] = ClientAllowChallengeMessage.receive;
idToClassMapper[MessageType.ClientStatus] = ClientStatusMessage.receive;
idToClassMapper[MessageType.ChallengeStatus] = ChallengeStatusMessage.receive;
idToClassMapper[MessageType.NotifyChallenged] = NotifyChallengedMessage.receive;
idToClassMapper[MessageType.ClientExitMatch] = ClientExitMatchMessage.receive;
idToClassMapper[MessageType.MatchEnd] = MatchEndMessage.receive;
idToClassMapper[MessageType.EnergyUpdated] = EnergyUpdatedMessage.receive;
idToClassMapper[MessageType.WinnerRatingsUpdated] = WinnerRatingsUpdatedMessage.receive;
idToClassMapper[MessageType.ThreadSelect] = ThreadSelectMessage.receive;
idToClassMapper[MessageType.PostDrawn] = PostDrawnMessage.receive;
idToClassMapper[MessageType.NewPostDone] = NewPostDoneMessage.receive;
idToClassMapper[MessageType.PostDecayed] = PostDecayedMessage.receive;
idToClassMapper[MessageType.TargetRequired] = TargetRequiredMessage.receive;
idToClassMapper[MessageType.ServerGoingDown] = ServerGoingDownMessage.receive;
idToClassMapper[MessageType.ChatMessage] = ChatMessageMessage.receive;

idToClassMapper[MessageType.ActionFinished] = ActionFinished.receive;
idToClassMapper[MessageType.ActionPlayCard] = ActionPlayCard.receive;
idToClassMapper[MessageType.ActionAttack] = ActionAttack.receive;
idToClassMapper[MessageType.ActionPost] = ActionDoPost.receive;

