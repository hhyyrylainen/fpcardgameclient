import { Pos2 } from './types';

class Config{
    // Everything not related to size
    readonly version = "0.0.5";

    readonly FpRed = "#C00";

    readonly FpBlue = "#28f";

    readonly FpGreen = "#efffbd";

    readonly gray = "#5c5c5c";

    readonly VictoryGold = "#e6ac00";

    readonly TurnName = Object.freeze({
        OWN: "Player's turn",
        OPPONENT: "Opponent's turn"})

    readonly AINames = Object.freeze({
        EASY: "[AI:1] Easy",
        MEDIUM: "[AI:2] Medium",
        HARD: "[AI:3] Hard",
        BEATME: "[AI:4] Punish Me Daddy"});

    // Server sent settings
    ratingsToWin = 100;


    // Draw order stuff //
    readonly DRAW_ORDER_BACKGROUNDS = 1000;
    readonly DRAW_ORDER_CARDS = 100000;
    readonly DRAW_ORDER_FLOATING_NUMBER = 1100000;


    // Global game size settings

    // At which width the Base scales are used
    readonly CARD_HEIGHT = 0.14;

    readonly CARD_INFO_HEIGHT = 0.43;

    readonly FIELD_CARD_HEIGHT = 0.10;

    readonly CARD_ASPECT_RATIO = 0.68;

    readonly HAND_MARGIN = 5;
    readonly HAND_PADDING = 2;

    readonly FIELD_MARGIN = 4;

    readonly BANCAMP_MARGIN = 3;
    readonly BANCAMP_PADDING = 2;


    // Calculated values
    cardWidth = 0;
    cardHeight = 0;

    cardPopupWidth = 0;
    cardPopupHeight = 0;

    fieldCardHeight = 0;
    fieldCardWidth = 0;


    width = 0;
    height = 0;

    // Modifier for hand-defined font sizes
    fontMod = 1;


    // Returns actual font size
    font(pxSize: number): number{

        return Math.floor(pxSize * this.fontMod);
    }

    // Returns the center pixel
    center(): Pos2{

        return new Pos2(this.width / 2, this.height / 2);
    }

    updateGameWidth(width: number, height: number){

        this.width = width;
        this.height = height;

        this.fontMod = this.height / 720;

        this.cardHeight = this.height * this.CARD_HEIGHT;
        this.cardWidth = this.cardHeight * this.CARD_ASPECT_RATIO;

        this.cardPopupHeight = this.height * this.CARD_INFO_HEIGHT;
        this.cardPopupWidth = this.cardPopupHeight * this.CARD_ASPECT_RATIO;

        // Card on battlefield size //
        this.fieldCardHeight = this.height * this.FIELD_CARD_HEIGHT;
        this.fieldCardWidth = this.fieldCardHeight * this.CARD_ASPECT_RATIO;
    }
}

let config = new Config();

export default config;



