import config from "./config";
import { Rect } from "./types";
import { Renderable } from "./drawable";

export class WinScreen extends Renderable{

    constructor(
        // Things to show on the screen
        public message: string, public won: boolean, public score: number)
    {
        super(new Rect(0.25, 0.4, 0.5, 0.3));
    }

    draw(ctx: CanvasRenderingContext2D, delta: number): boolean{


        ctx.fillStyle = "rgba(15, 15, 15, 0.7)";

        // Background
        ctx.fillRect(this.pixelRect.x, this.pixelRect.y,
                     this.pixelRect.width, this.pixelRect.height);

        ctx.font = "bold " + config.font(34) + "px serif";
        ctx.textAlign = "center";

        let center = this.getPixelCenter();
        
        let textPosX = center.x;
        let textPosY = center.y - (this.pixelRect.height / 4);
        
        if(this.won){

            ctx.fillStyle = config.VictoryGold;
            ctx.fillText("VICTORY", textPosX, textPosY);

        } else {

            ctx.fillStyle = config.FpRed;
            ctx.fillText("DEFEAT", textPosX, textPosY);
        }

        // Message //
        ctx.font = config.font(28) + "px serif"
        ctx.fillStyle = "white";
        ctx.fillText(this.message, textPosX, textPosY + config.font(34) + 15);

        return false;
    }
}
