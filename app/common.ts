import { Pos2, Rect } from "./types";


/**
   Decodes an URL parameter in the form ?param=value
*/
export function getURLParameter(name: string): string{
    let match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

/**
   Returns the address of the game server based on the current url
*/
export function getServerAddress(): string{
    if (document.location.hostname == "localhost"){

        return "ws://" + window.location.hostname + ":8080/ws";

    } else {

        if (window.location.protocol != "https:") {
            
            return "ws://" + window.location.hostname + "/ws";
            
        } else {

            return "wss://" + window.location.hostname + "/wss";
        }
    }
}


export function isPointInside(point: Pos2, rect: Rect): boolean{

    if(point.x < rect.x)
        return false;

    if(point.y < rect.y)
        return false;

    if(point.y > rect.y + rect.height)
        return false;

    if(point.x > rect.x + rect.width)
        return false;
    
    return true;
}

