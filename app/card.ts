
import { Pos2, Rect } from "./types";

import { Renderable, drawTextWithBorder } from "./drawable";

import resourcemanager from "./resourcemanager";

import config from "./config";
import { isPointInside } from "./common";
import { easeOutBounce, easeInOutCubic, easeOutQuart, easeInQuart } from "./easing";

export enum CardType{

    User,
    Spell
}

//! Holds data for cards
class CardDataHolder{

    //! All the cards for which we have received the data for
    knownCards: { [key: number]: CardData } = {};

    receiveNewDefinition(definition: any){

        console.log("Received definition for card type " + definition["id"]);

        // Load the card image //
        let imgReceive: CardData = null;
        let img = new Image();

        if(definition["type"] == "spell"){

            imgReceive = new SpellData(
                definition["id"] as number,
                definition["image"] as string,
                definition["description"] as string,
                definition["name"] as string,
                definition["cost"] as number,
                img);

            
        } else if(definition["type"] == "user"){

            imgReceive = new UserCardData(
                definition["id"] as number,
                definition["image"] as string,
                definition["description"] as string,
                definition["name"] as string,
                definition["attack"] as number,
                definition["defence"] as number,
                definition["cost"] as number,
                img);

        } else {

            throw "received card definition of unknown type: " +
                JSON.stringify(definition);
        }

        if(!imgReceive)
            throw "receive new definition didn't successfully create CardData object";

        // Add the created card info object //
        this.knownCards[definition["id"]] = imgReceive;

        img.onload = () => {
            imgReceive.imageLoaded = true;
        };
        img.src = definition["image"];
    }

    /** Creates a card object from received data
     */
    receiveCard(jsonblob: any): Card{

        let card = new Card(jsonblob["matchcard"] as number);
       
        // Reveal card immediately if it is ours //
        if(!jsonblob["hidden"]){

            // Own card //
            card.own = true;

            card.reveal(jsonblob, this);
        }

        return card;
    }

}

/** Properties of a card type
 */
class CardData{

    /** True once this card is ready to display
     */
    imageLoaded: boolean = false;

    constructor(public cardType: CardType,
                public imgObj: HTMLImageElement,
                public id: number,
                public name: string,
                public description: string,
                /** Url of the image
                 */
                public image: string)
    {

    }
    
}

/** Spell card
 */
class SpellData extends CardData{

    constructor(id: number, image: string, description: string, name: string,
                public cost: number,
                public img: HTMLImageElement)
    {
        super(CardType.Spell, img, id, name, description, image);
    }
}

/** Non-spell card
 */
class UserCardData extends CardData{

    constructor(id: number, image: string, description: string, name: string,
                public attack: number,
                public defence: number,
                public cost: number,
                public img: HTMLImageElement)
    {
        super(CardType.User, img, id, name, description, image);
    }
}

/**
   Main card class
*/
export class Card extends Renderable{

    /**
       Location this card returns to when not animating
    */
    
    home: Pos2

    /**
       True if this card is face down
    */
    hiddenFace: boolean = true;

    /**
       ID of card type, valid if hiddenFace = false
    */
    typeID: number = -1;

    /**
       Card type properties. Valid if typeID != -1
    */
    typeInfo: CardData = null;
    
    /**
       True if this is the local player's card
     */
    own: boolean = false;

    /**
       True when hovered over and should show
    */
    hovered: boolean = false;
    
    /**
       True when the player has grabbed drawncard card and is waving it around
    */
    dragged: boolean = false;

    /**
       True if not in a player's hand. if true visible should also be true. unless this is
       a trap or something
    */
    inPlay: boolean = false;

    /**
       True if the card is in bancamp
    */
    banned: boolean = false;

    /**
       True if hidden in game and should be ignored
    */
    occluded: boolean = false;

    /**
       True if this card is no longer good for anything (it is voided / discarded)
    */
    voided: boolean = false;

    /**
       Rendering scale for this card
    */
    scale: number = 1.0;

    // Animation data //
    /**
       True when animating and animations should be updated
    */
    animating: boolean = false;
    animationObj: CardAnimation = null;


    // Gameplay data //
    /**
       True once a card has been used to attack this turn
    */
    attackedThisTurn: boolean = false;
    
    attack: number = 0;
    defence: number = 0;
    cost: number = 0;

    constructor(
        /**
           ID of this instance of the card
        */
        public readonly matchID: number
    )
    {
        super(new Rect(0, 0, 0, 0));
        this.pixelRect = this.rect;
    }

    /**
       Makes this card face up
    */
    reveal(jsonblob: any, holder: CardDataHolder){

        this.hiddenFace = false;
        this.typeID = jsonblob["id"];

        if(jsonblob["full"]){

            holder.receiveNewDefinition(jsonblob["definition"]);
            
        } else {

            // Error if not exists //
            if(holder.knownCards[this.typeID] === undefined){

                console.error("Received a non-full card before getting the " +
                              "full definition");
            }
        }

        this.typeInfo = holder.knownCards[this.typeID];
        
        this.setCardProperties();
    };

    /**
       Gets the image for this card. Either the face down image or the loaded card face
    */
    getCardImage(): HTMLImageElement{

        // Face image //
        if(!this.hiddenFace && this.typeInfo.imageLoaded)
            return this.typeInfo.imgObj;

        // Face down image //
        return resourcemanager.cardFaceDownImg;
    }

    /**
       Draws the card
    */
    draw(ctx: CanvasRenderingContext2D, delta: number): boolean{

        // Update animation //
        if(this.animating)
            this.updateAnimation(delta);

        if(this.occluded)
            return false;

        const cardImage = this.getCardImage();
        let rect = this.getRect();
        
        // Draw transparent image to where the card was dragged from //
        if(this.dragged){

            let rect = this.getRect(true);

            ctx.globalAlpha = 0.6;

            ctx.drawImage(cardImage, rect.x, rect.y,
                          rect.width, rect.height);

            ctx.globalAlpha = 1;
        }

        // Draw highlight if applicable //

        // Draw background if own and is banned
        if(this.banned && this.own){

            ctx.fillStyle = config.FpRed;
            ctx.fillRect(rect.x - 1, rect.y - 1,
                         rect.width + 2, rect.height + 2);
        }

        // Draw the card image //
        ctx.drawImage(cardImage, rect.x, rect.y,
                      rect.width, rect.height);
        
        if(!this.hiddenFace){

            // Draw the properties as this is visible //
            this.drawProperties(ctx, rect);
        }

        return false;
    }

    // Card property number config //
    static MULTIPLY_SCALE = 2;
    static BORDER_BASE = 8;

    /**
       Draws a number on top of an image, used to draw properties
    */
    drawNumberInRect(ctx: CanvasRenderingContext2D, rect: Rect, text: string,
                     img: HTMLImageElement, colour: string, fontsize: number,
                     renderScale: number, borderColour: string,
                     )
    {
        let border = Card.BORDER_BASE;
        
        if(fontsize < 30){

            border = 4;
        }
        
        // Background image //
        ctx.drawImage(img, rect.x, rect.y,
                      rect.width, rect.width);
        
        ctx.save();

        ctx.textAlign = "center";
        ctx.textBaseline = "top";
        
        ctx.scale(renderScale, renderScale);

        drawTextWithBorder(ctx, text,
                           new Pos2((rect.x + rect.width / 2) *
                                    Card.MULTIPLY_SCALE,
                                    (rect.y) * Card.MULTIPLY_SCALE),
                           colour,
                           borderColour,
                           border * Card.MULTIPLY_SCALE);


        ctx.restore();

    }

    /**
       Draws the properties of a card, like the attack, defence and cost
    */
    drawProperties(ctx: CanvasRenderingContext2D, rect: Rect){

        // Cannot work on face down card //
        if(this.hiddenFace)
            throw "trying to draw properties of a face down card";

        let fontsize = rect.width / 4;
        
        let renderScale = 1 / Card.MULTIPLY_SCALE;

        ctx.font = "bold " + (fontsize * Card.MULTIPLY_SCALE) + "px serif";


        let img: HTMLImageElement;
        let colour: string;
        
        // User card properties //
        if(this.typeInfo.cardType == CardType.User){

            // Attack //
            let attackRect = new Rect(rect.x, rect.y, rect.width / 6, 0);

            if(!this.attackedThisTurn){
                // Active sword //
                img = resourcemanager.activeSword;
                colour = config.FpRed;
                
            } else {

                img = resourcemanager.disabledSword; 
                colour = "black";
            }

            this.drawNumberInRect(
                ctx, attackRect, ""+this.attack,
                img, colour, fontsize, renderScale, "white"
            );

            // Defence //
            let defenceRect = new Rect(rect.x + rect.width - (rect.width / 6),
                                       rect.y,
                                       rect.width / 6, 0);
            
            img = resourcemanager.normalShield;

            if(this.defence == (this.typeInfo as UserCardData).defence){
                colour = "#66ff33";
            } else {
                colour = "red";
            }

            this.drawNumberInRect(
                ctx, defenceRect, ""+this.defence,
                img, colour, fontsize, renderScale, "#999999"
            );
        }
        
        // Cost //
        let costRect = new Rect(rect.x + rect.width - (rect.width / 6),
                                rect.y + rect.height / 1.8,
                                rect.width / 6, 0);
        
        img = resourcemanager.normalEnergy;

        // TODO: can player afford the cost
        if(this.inPlay){

            colour = "white";
            
        } else {

            //ctx.fillStyle = "white";
            colour = "black";
        }


        this.drawNumberInRect(
            ctx, costRect, ""+this.cost,
            img, colour, fontsize, renderScale, "#394d00"
        );
    }

    /**
       Draw popups, drawn after all other cards. Only if wanted
    */
    drawPopup(ctx: CanvasRenderingContext2D, delta: number): void{

        if(this.occluded)
            return;

        if(!this.hovered || this.dragged)
            return;

        const cardImage = this.getCardImage();
        let rect = this.getRect();

        let sideoffset = (rect.width / 2) + rect.x -
            (config.cardPopupWidth / 2);

        // Make sure it doesn't go off the edges of the screen //
        if(sideoffset < 0){

            // Would go over the left side //
            sideoffset = 0;
            
        } else if(sideoffset + (config.cardPopupWidth / 2) > config.width){

            // Would go over the right side //
            sideoffset = config.width - config.cardPopupWidth;
        }

        let topoffset = 0;

        // Check should it render under the card //
        if(rect.y - config.cardPopupHeight < 0){

            // Draw under the card //
            topoffset = rect.y + rect.height + 1;

        } else {

            // Draw above the card //
            topoffset = rect.y - 1 - config.cardPopupHeight;
        }

        // This won't properly render when against the edge of the screen, but it's good
        // enough
        ctx.fillStyle = "red";
        ctx.fillRect(sideoffset - 2, topoffset - 2,
                     config.cardPopupWidth + 4, config.cardPopupHeight + 4);

        // Draw image //
        ctx.drawImage(cardImage, sideoffset, topoffset,
                      config.cardPopupWidth, config.cardPopupHeight);

        if(!this.hiddenFace){
            
            // Draw properties //
            this.drawProperties(ctx, new Rect(sideoffset, topoffset,
                                              config.cardPopupWidth, config.cardPopupHeight));
        }
    }

    onResolutionUpdated(){

    }

    /**
       Gets the cards target position on screen in pixels
    */
    getRect(athome: boolean = false): Rect{

        let basePos = athome ? this.home : this.pixelRect.getPos2();

        if(this.inPlay)
            return new Rect(basePos.x, basePos.y,
                            config.fieldCardWidth * this.scale,
                            config.fieldCardHeight * this.scale);
        else
            return new Rect(basePos.x, basePos.y,
                            config.cardWidth * this.scale,
                            config.cardHeight * this.scale);
    }

    /**
       Sets the home position of this card
    */
    setHome(pos: Pos2){

        this.home = pos;
    }

    /**
       Snaps this card's position to its home position
    */
    setToHome(){

        this.pixelRect.x = this.home.x;
        this.pixelRect.y = this.home.y;
    }

    /**
       Sets the temporary position
    */
    setPos(pos: Pos2){

        this.pixelRect.setPos2(pos);
    }

    isPointInside(pos: Pos2){

        return isPointInside(pos, this.getRect());
    }

    // Reads properties of this card type and applies them to this instance
    setCardProperties(){

        if(!this.typeInfo)
            throw "Cannot read properties: cardType not set";

        if(this.typeInfo.cardType == CardType.User){

            let info = this.typeInfo as UserCardData;
            
            this.attack = info.attack;
            this.defence = info.defence;
            this.cost = info.cost;
            
        } else if(this.typeInfo.cardType == CardType.Spell){

            let info = this.typeInfo as SpellData;
            
            this.cost = info.cost;

        } else {

            throw "setCardproperties unkown card type";
        }
        

    }

    /**
       Adds an animation to this card to move home. Doesn't replace current animation
       if overrideMove is false
    */
    moveHome(timems: number, overrideMove: boolean = false){

        if(!timems)
            timems = 100;
        

        // Only set home if not animating or being dragged //
        if(this.dragged){

            return;
        }

        if(this.animating && !overrideMove)
            return;

        // Check how far away the actual position is from the target //
        let offby = Math.abs(this.pixelRect.x - this.home.x) +
            Math.abs(this.pixelRect.y - this.home.y);

        if(offby < 1)
            return;
        
        // Add an animation if farther than a few pixels //
        if(offby > 10){
            
            this.setAnimation(new CardAnimationMoveToHome(timems));
            
        } else {

            this.pixelRect.x = this.home.x;
            this.pixelRect.y = this.home.y;
        }

    }
    
    /**
       Replaces current animation
    */
    setAnimation(animation: CardAnimation){

        if(!animation){

            this.animating = false;
            this.animationObj = null;
            return;
        }
        
        this.animating = true;
        this.animationObj = animation;
    }

    /**
       Updates animation
    */
    updateAnimation(delta: number){

        if(!this.animationObj){
            
            // No active animations, disable calling this //
            this.animating = false;
            return;
        }

        if(this.animationObj.update(delta, this)){

            // Animation finished //

            // Callback //

            this.animationObj = null;
            this.animating = false;

            // Move home if not at home //
            if(!this.pixelRect.getPos2().equals(this.home))
                this.moveHome(100, false);
            
            return;
        }
    }


    
}

export abstract class CardAnimation{

    /**
       Updates this animation and applies the current state to target

       delta is milliseconds since the last frame
       
       @returns True if finished
    */
    abstract update(delta: number, target: Card): boolean;
}

abstract class CardAnimationFunction extends CardAnimation{

    /**
       Time passed in total
    */
    protected passed: number = 0;

    /**
       Holds the starting position
    */
    protected origin: Pos2 = null;

    constructor(
        /**
           Duration of this animation in milliseconds
        */
        protected durationMS: number)
    {
        super();

        if(durationMS <= 0){
            
            throw "0 length animation";
        }
    }

    /**
       Takes in a linear progress in range 0.0 - 1.0 and returns another value in the same
       range. This is used for easings. By default just returns input value
    */
    progress(linearProgress: number): number{

        return linearProgress;
    }

    /**
       Returns the target position of the animation
    */
    abstract getTarget(target: Card): Pos2;

    /**
       Returns the origin point. Called once when the animation starts.
       Can be used to reset the card to some position
    */
    getOrigin(target: Card): Pos2{

        return target.getRect().getPos2();
    }

    /**
       Called when finished. Return false to stop actually being finished. True to end
       this animation. Can be used to chain animations
    */
    onFinish(target: Card, delta: number): boolean{

        return true;
    }

    update(delta: number, target: Card): boolean{

        this.passed += delta;

        if(this.passed >= this.durationMS){

            // Finished //
            return this.onFinish(target, delta);
        }

        if(!this.origin){

            this.origin = this.getOrigin(target);
        }

        // Linear move towards //
        let progress = this.passed / this.durationMS;

        // Apply easings
        progress = this.progress(progress);

        target.setPos(this.origin.interpolate(this.getTarget(target), progress));

        return false;
    }
    
}

export class CardAnimationMoveToHome extends CardAnimationFunction{

    constructor(
        /**
           The number of milliseconds the card should take to reach its home position
        */
        durationMS: number)
    {
        super(durationMS);
    }
    
    /**
       Easing
    */
    progress(t: number): number{

        return easeInOutCubic(t);
    }

    getTarget(target: Card): Pos2{

        return target.home;
    }

    onFinish(target: Card): boolean{

        target.setToHome();
        return true;
    }
}

export class CardAnimationAttact extends CardAnimationFunction{

    /**
       Holds the return animation
    */
    returnAnimation: CardAnimationMoveToHome = null;
    
    constructor(public targetPos: Pos2,
                /**
                   How long the card should take to reach its target
                */
                durationMS: number,
                /**
                   How long should the card take to return home
                */
                returnHomeMS: number = 350)
    {
        super(durationMS);

        this.returnAnimation = new CardAnimationMoveToHome(returnHomeMS);
    }

    /**
       Easing
    */
    progress(t: number): number{

        return easeInQuart(t);
        //return easeOutBounce(t);
    }

    getTarget(target: Card): Pos2{

        return this.targetPos;
    }

    /**
       Chains the return animation
    */
    onFinish(target: Card, delta: number): boolean{

        //return true;
        return this.returnAnimation.update(delta, target);
    }
}

export let cardholder = new CardDataHolder();

