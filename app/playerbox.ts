import { Renderable, drawTextWithShadow, drawOutline } from "./drawable";
import { Rect, Pos2 } from "./types";

import {  } from "./common";
import resourcemanager from "./resourcemanager";
import config from "./config";

/**
   Shows player and score info
*/
export class PlayerInfoBox extends Renderable{

    constructor(public playerName: string,
                public ownScore: number, public opponentsScore: number,
                public ownEnergy: number, public opponentsEnergy: number)
    {
        super(new Rect(0.0015, 0.001, 0.29, 0.15));
    }
    
    draw(ctx: CanvasRenderingContext2D, delta: number): boolean{

        // Background
        ctx.fillStyle = resourcemanager.tileBlackGray;
        
        ctx.fillRect(this.pixelRect.x, this.pixelRect.y,
                     this.pixelRect.width, this.pixelRect.height);
        
        // Border
        drawOutline(ctx, this.getRect(), "black", false);

        // Player name
        ctx.textAlign = "left";
        let px = Math.floor(this.pixelRect.height / 6);
        ctx.font = "bold " + px + "px serif";

        drawTextWithShadow(ctx, this.playerName, new Pos2(
            this.pixelRect.x + 5, this.pixelRect.y + px + 5),
                           "white", 2, "rgba(0, 0, 0, 0.7)");
        
        ctx.textAlign = "right";
        drawTextWithShadow(ctx, "Rank: 0", new Pos2(
            (this.pixelRect.x + this.pixelRect.width) - 5, this.pixelRect.y + px + 5),
                           "white", 2, "rgba(0, 0, 0, 0.7)");
        
        // 'You' and 'Opponent' strings
        ctx.font = "bold " + Math.floor(px * 0.8) + "px serif";
        ctx.textAlign = "left";
        ctx.fillText("You", this.pixelRect.x + 5, this.pixelRect.y + ( px * 2) + 40);

        ctx.fillText("Opponent", this.pixelRect.x + 5, this.pixelRect.y + ( px * 3.5) + 40);
        
        // Score strings
        ctx.font = Math.floor(px * 0.5) + "px serif";
        ctx.textAlign = "center";

        ctx.fillText("Winner Ratings (" + config.ratingsToWin +
                     " to Win)", this.pixelRect.x + ( this.pixelRect.width / 3 ),
                     this.pixelRect.y + px + 35);

        ctx.fillText("Energy",
                     this.pixelRect.x + this.pixelRect.width - ( this.pixelRect.width / 4 ),
                     this.pixelRect.y + px + 35);

        // Actual numbers
        ctx.font = "bold " + px + "px serif";

        drawTextWithShadow(ctx, `${this.ownScore}`,
                           new Pos2(this.pixelRect.x + ( this.pixelRect.width / 3 ),
                                    this.pixelRect.y + ( px * 2) + 40),
                            this.ownScore > this.opponentsScore ? config.VictoryGold :
                            config.FpRed,
                           1, "rgba(0, 0, 0, 0.4)");


        drawTextWithShadow(ctx, `${this.opponentsScore}`,
                           new Pos2(this.pixelRect.x + ( this.pixelRect.width / 3 ),
                                    this.pixelRect.y + ( px * 3.5) + 40),
                           this.opponentsScore > this.ownScore ? config.VictoryGold :
                           config.FpRed,
                           1, "rgba(0, 0, 0, 0.4)");
        

        // Energy
        drawTextWithShadow(ctx, `${this.ownEnergy}`,
                           new Pos2(this.pixelRect.x + this.pixelRect.width -
                                    ( this.pixelRect.width / 4 ),
                                    this.pixelRect.y + ( px * 2) + 40),
                           this.ownEnergy > 0 ? "Aquamarine" : "white",
                           1, "rgba(0, 0, 0, 0.6)");

        drawTextWithShadow(ctx, `${this.opponentsEnergy}`,
                           new Pos2(this.pixelRect.x + this.pixelRect.width -
                                    ( this.pixelRect.width / 4 ),
                                    this.pixelRect.y + ( px * 3.5) + 40),
                           this.opponentsEnergy > 0 ? "Aquamarine" : "white",
                           1, "rgba(0, 0, 0, 0.6)");
        
        return false;
    }
}

