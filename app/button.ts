import { Pos2, Rect } from "./types";
import config from "./config";
import { Renderable, drawOutline, ClickableThing } from "./drawable";
import { Card } from "./card";

/**
   Callback type for Button
*/
export declare type ClickHandler = (clicked: Button) => void;


export class Button extends Renderable implements ClickableThing{

    
    /**
       True when mouse is over this and should highlight this
    */
    hovered: boolean = false;

    /**
       If false won't accept clicks and is rendered as disabled
    */
    enabled: boolean = true;

    /**
       Called when clicked
    */
    onClicked: ClickHandler;



    constructor(rect: Rect,
                public text: string,
                clicked: ClickHandler,
                // Visual style //
                public backgroundFill: any,
                public hoveredFill: any,
                public disabledFill: any,
                public textColour: string,
                public font: string,
               )
    {

        super(rect);

        this.onClicked = clicked;
    }

    /**
       Returns true and runs the callback if position is within this button
    */
    clickCheck(pos: Pos2): boolean{

        if(!this.isPointInside(pos))
            return false;

        if(this.enabled){

            this.onClicked(this);
        }
        
        return true;
    }


    mouseMove(pos: Pos2): boolean{

        if(!this.enabled){

            this.hovered = false;
        }

        if(this.isPointInside(pos)){

            this.hovered = false;
            
        } else {

            this.hovered = true;
        }

        return false;
    }

    draw(ctx: CanvasRenderingContext2D, delta: number): boolean{

        if(this.enabled){

            if(!this.hovered){
                
                ctx.fillStyle = this.backgroundFill;
                
            } else {

                ctx.fillStyle = this.hoveredFill;
            }
            
        } else {

            ctx.fillStyle = this.disabledFill;
        }

        ctx.fillRect(this.pixelRect.x, this.pixelRect.y,
                     this.pixelRect.width, this.pixelRect.height);

        // Outline //
        drawOutline(ctx, this.getRect(), "black", false);
        

        ctx.fillStyle = this.textColour;
        ctx.font = this.font;

        ctx.textAlign = "center";

        let center = this.getPixelCenter();
        
        ctx.fillText(this.text, center.x, center.y);

        return false;
    }
}

