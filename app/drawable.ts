import config from "./config";
import { isPointInside } from "./common";
import { Rect, Pos2 } from "./types";


/**
   Base class for anything that is drawn on a context
*/
export abstract class Drawable{

    /**
       @param delta The number of milliseconds passed since last frame
       @returns True this object shouldn't be drawn anymore. Based on from where
       this is called the object will be removed from there
    */
    abstract draw(ctx: CanvasRenderingContext2D, delta: number): boolean
    
}

/**
   Base for renderable things that that have base geometry of a box and ordering value
*/
export abstract class Renderable extends Drawable{

    protected rect: Rect;
    protected pixelRect: Rect;
    public drawOrder: number;

    constructor(relativeRect: Rect, draworder: number = 0){

        super();

        this.drawOrder = draworder;
        this.rect = relativeRect;
        this.pixelRect = new Rect(0, 0, 0, 0);

        this.onResolutionUpdated();
    }

    draw(ctx: CanvasRenderingContext2D, delta: number): boolean{

        drawOutline(ctx, this.pixelRect, "black", false);
        return false;
    }

    /**
       Recalculates pixel positions
    */
    onResolutionUpdated(){
        
        this.pixelRect.x = this.rect.x * config.width
        this.pixelRect.y = this.rect.y * config.height
        this.pixelRect.width = this.rect.width * config.width
        this.pixelRect.height = this.rect.height * config.height
    }

    /**
       Returns the logical position
    */
    getRelativeRect(): Rect{

        return this.rect;
    }
    
    /**
       Returns the actual pixel position
    */
    getRect(): Rect{

        return this.pixelRect;
    }
    
    getPixelCenter(): Pos2{

        return new Pos2(this.pixelRect.x + (this.pixelRect.width / 2),
                        this.pixelRect.y + (this.pixelRect.height / 2));
    }

    /**
       Transforms a position from relative to this object to absolute
    */
    relativeToAbsolute(pos: Pos2): Pos2{

        let rect = this.getRect();
        return new Pos2(rect.x + pos.x, rect.y + pos.y);
    }

    /**
       Returns true if the point is inside this object's Rect
    */
    isPointInside(point: Pos2): boolean{

        return isPointInside(point, this.getRect());
    }


}

export function sortRenderables(objs: Array<Renderable>): void{

    // Skip sorting if already sorted //
    let sorted = true;

    for(let i = 1; i < objs.length; ++i){
        
        if(objs[i - 1].drawOrder > objs[i].drawOrder){

            sorted = false;
            break;
        }
    }

    if(sorted)
        return;

    objs.sort((a: Renderable, b: Renderable):number => {

        if(a.drawOrder < b.drawOrder)
            return 1;

        if(b.drawOrder > a.drawOrder)
            return -1;

        return 0;
    });
}

/**
   Font size class. Used to pass font properties around
*/
export class FontDef{

    constructor(public fontSize: number,
                public fontApply: (ctx: CanvasRenderingContext2D) => void)
    {

    }

    isSizeSame(size: number): boolean{

        return this.fontSize == size;
    }
}


// Common draw functions //

export function drawOutline(ctx: CanvasRenderingContext2D, rect: Rect,
                            colour: string, fill: boolean)
{
    ctx.lineWidth = 3;

    let radius = {tl: 5, tr: 5, br: 5, bl: 5};

    ctx.beginPath();
    ctx.moveTo(rect.x + radius.tl, rect.y);
    ctx.lineTo(rect.x + rect.width - radius.tr, rect.y);
    ctx.quadraticCurveTo(rect.x + rect.width, rect.y, rect.x + rect.width,
                         rect.y + radius.tr);
    ctx.lineTo(rect.x + rect.width, rect.y + rect.height - radius.br);
    ctx.quadraticCurveTo(rect.x + rect.width, rect.y + rect.height,
                         rect.x + rect.width - radius.br, rect.y + rect.height);
    ctx.lineTo(rect.x + radius.bl, rect.y + rect.height);
    ctx.quadraticCurveTo(rect.x, rect.y + rect.height, rect.x,
                         rect.y + rect.height - radius.bl);
    ctx.lineTo(rect.x, rect.y + radius.tl);
    ctx.quadraticCurveTo(rect.x, rect.y, rect.x + radius.tl, rect.y);
    ctx.closePath();
    
    if (fill) {

        ctx.fillStyle = colour;
        ctx.fill();
        
    } else {
        
        ctx.strokeStyle = colour;
        ctx.stroke();
    }
}

export function drawTextWithShadow(ctx: CanvasRenderingContext2D, text: string, pos: Pos2,
                                   colour: string, shadowOffset: number, shadowColour: string)
{
    // Shadow //
    ctx.fillStyle = shadowColour;
    ctx.fillText(text, pos.x + shadowOffset, pos.y + shadowOffset);

    // Actual text //
    ctx.fillStyle = colour;
    ctx.fillText(text, pos.x, pos.y);
}

export function drawTextWithBorder(ctx: CanvasRenderingContext2D, text: string, pos: Pos2,
                                   colour: string, borderColour: string, borderWidth: number)
{

    ctx.miterLimit = 2;
    
    ctx.strokeStyle = borderColour;
    ctx.lineWidth = borderWidth;

    ctx.strokeText(text, pos.x, pos.y);

    ctx.fillStyle = colour;
    ctx.fillText(text, pos.x, pos.y);
}


/**
   Thing that can be clicked
*/
export interface ClickableThing{

    /**
       Animates this when mouse hovers over this

       @returns True if event propagation should stop
    */
    mouseMove(pos: Pos2): boolean;

    /**
       Returns true if pos is inside this object
    */
    clickCheck(pos: Pos2): boolean;
}

