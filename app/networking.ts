import * as $ from 'jquery';

import { constructMessageFromJson, BaseMessage, HelloMessage, MessageType } from "./messages";
import { getServerAddress } from "./common";

import { game } from "./gamemain";


export class Networking{

    private socket: WebSocket;

    serverURL: string;

    /**
       When true waiting for specific message before handling normal messages
    */
    waitingPostInit: boolean = true;

    constructor(){

        // Detecting the game port //
        this.serverURL = getServerAddress();
        
        console.log("Opening connection to server '" + this.serverURL + "'");

        try {
            
            this.socket = new WebSocket(this.serverURL);

            this.socket.onopen = this.onOpen;
            this.socket.onmessage = this.onMessage;
            this.socket.onerror = this.onError;
            this.socket.onclose = this.onClose;
            
        } catch(err) {

            game.doErrorRedirect(err.message);
            return;
        }
    }

    /**
       Sends a properly formated message
    */
    send(obj: BaseMessage){

        this.socket.send(JSON.stringify(obj));
    }

    // Socket functions //
    private onOpen = () =>{

        let plyname = game.getPlayerName();
        
        console.log("Connecting as " + plyname);
        
        // Do rest of init //
        this.send(new HelloMessage(plyname));
    }

    private onError = (ev: ErrorEvent) => {

        this.socket.onclose = null; 
        
        console.log("Socket error: " + ev.message);
        
        if(ev.message == undefined)
            ev.message = "Server is Offline or not Responding";

        game.doErrorRedirect("Game server connection failed: " + ev.message);
    }
    
    private onClose = (ev: CloseEvent) => {

       	console.log('Socket Status: ' + this.socket.readyState + ' (Closed)');

        $("#LostConnectionDialog").dialog().show();
    }			

    private onMessage = (msg: MessageEvent) => {

        let typedMessage: BaseMessage;
        
        try{
            // Parse it //
            typedMessage = constructMessageFromJson(JSON.parse(msg.data));
            
            if(!typedMessage)
                throw "Failed to create message object";
            
            if(this.waitingPostInit && (
                typedMessage.id != MessageType.Hello && 
                    // Also accept errors
                    typedMessage.id != MessageType.Error))
            {
                
                throw "Networking is waiting for hello, but got something else";
            }
            
        } catch(e){

            console.error(`Invalid message, error: ${e}, message data:` + msg.data);
        }

        // Handle it //
        typedMessage.handle();
    }
}


