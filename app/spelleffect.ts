
import { Renderable } from "./drawable";
import { Rect, Pos2 } from "./types";

var Victor = require("victor");

/**
   Represents an image that flys towards a position (and maybe scales up or down)
*/
export class SpellEffect extends Renderable{

    autoRotate: boolean = true;
    rotationOffset: number = -(Math.PI / 2);

    constructor(public image: HTMLImageElement,
                /**
                   The time in milliseconds this effect should last
                */
                private duration: number, private startPosAndSize: Rect, private target: Pos2)
    {
        
        super(startPosAndSize);
        this.pixelRect = startPosAndSize;
    }
    
    draw(ctx: CanvasRenderingContext2D, delta: number): boolean{

        this.duration -= delta;

        if(this.duration <= 50)
            return true;

        // Calculate required speed to reach target in the remaining time
        let distance = new Victor(this.target.x - this.pixelRect.x,
                                  this.target.y - this.pixelRect.y);

        let requiredSpeed = distance.length() / this.duration;


        let velocity = distance.normalize().multiply(new Victor(requiredSpeed, requiredSpeed));


        // Apply velocity //
        this.pixelRect.x += velocity.x * delta;
        this.pixelRect.y += velocity.y * delta;

        // Draw
        ctx.save()

        ctx.translate(this.pixelRect.x, this.pixelRect.y);

        ctx.rotate(velocity.direction() + this.rotationOffset);

        ctx.drawImage(this.image, 0, 0, this.pixelRect.width, this.pixelRect.height);

        ctx.restore()
        
        return false;
    }

    onResolutionUpdated(){
    }
}



