import { Drawable } from "./drawable";
import { Pos2, Size2 } from "./types";

export class TimedText extends Drawable{

    // Number of milliseconds elapsed
    private timePassed: number = 0;

    constructor(public text: string, public aliveTime: number,
                public position: Pos2, public fontPXSize: number,
                // Fade out settings
                public fadeOutTime: number = 600,
                public fadeInEnd: number = 200)
    {
        super();
        
    }

    draw(ctx: CanvasRenderingContext2D, delta: number): boolean{

        this.timePassed += delta;

        if(this.timePassed > this.aliveTime)
            return true;

        if(this.timePassed >= this.fadeInEnd &&
           this.timePassed <= this.aliveTime - this.fadeOutTime)
        {
            ctx.fillStyle = "rgba(0, 0, 0, 1)";

        } else if(this.timePassed < this.fadeInEnd){

            // Fading in //
            let percentage = 1 - ((this.fadeInEnd - this.timePassed) / this.fadeInEnd);
            ctx.fillStyle = "rgba(0, 0, 0, " + percentage + ")";

        } else {

            // Fading out //
            let percentage = (this.aliveTime - this.timePassed) / this.fadeOutTime;

            ctx.fillStyle = "rgba(0, 0, 0, " + percentage + ")";                    
        }


        ctx.textAlign = "center";
        ctx.font = this.fontPXSize + "px serif";

        ctx.fillText(this.text, this.position.x, this.position.y);
        
        return false;
    }


}


