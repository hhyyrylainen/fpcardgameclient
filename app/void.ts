
import config from "./config";
import { CardAnimation, Card } from "./card";
import { Pos2 } from "./types";


export class VoidAnimation extends CardAnimation{

    static END_SIZE: number = 3.4;

    static SIZE_MULT: number = 3;

    speed: number = 5;
    size: number = 1;

    target: Pos2;

    constructor(){

        super();

        this.target = new Pos2((config.width / 2) + 20, config.height / 2.4);
    }

    update(delta: number, target: Card): boolean{

        this.speed += 0.001 * delta;

        // Stop once final size is reached
        this.size = this.size * (1 + (VoidAnimation.SIZE_MULT * (delta / 1000)))

        if(this.size < 0.1 || this.size > VoidAnimation.END_SIZE){

            // Hide the card
            target.occluded = true
            return true
        }

        let pos = target.getRect().getPos2();

        if(pos.x < this.target.x){

            pos.x += this.speed;
            
        } else {
            
            pos.x -= this.speed;
        }


        if(pos.y < this.target.y){

            pos.y += this.speed;

        } else {

            pos.y -= this.speed;
        }

        target.setPos(pos);
        target.scale = this.size;

        return false;
        
    }
}


export function voidCard(card: Card){

    card.voided = true;

    // Play void animation that hides the card //
    card.setAnimation(new VoidAnimation());
}


