import config from "./config";
import resourcemanager from "./resourcemanager";
import { Rect, Pos2 } from "./types";
import { Card } from "./card";
import { Renderable, drawOutline } from "./drawable";
import { game } from "./gamemain";


export class Battlefield extends Renderable{

    /**
       True when player is dragging a card onto the field
    */
    canDrop: boolean = false;

    // TODO: move these to rules
    ownCards: Array<Card> = [];
    opponentsCards: Array<Card> = [];

    constructor(){

        super(new Rect(0.3, 0.16, 0.4, 0.68));
    }
    
    addCard(own: boolean, card: Card){

        // Add the card //
        if(own){

            this.ownCards.push(card);
            
        } else {

            this.opponentsCards.push(card);
        }

        // Apply effects //
        game.rules.cardEnterPlay(card, this);
        
        this.repositionCards();
    }

    /**
       A card has been banned and leaves the field
    */
    removeCard(card: Card){

        let index = this.ownCards.indexOf(card);

        if(index > -1){

            this.ownCards.splice(index, 1);
            
        } else {

            index = this.opponentsCards.indexOf(card);

            if(index < 0)
                throw "field removeCard didn't find the card";

            this.opponentsCards.splice(index, 1);
        }

        card.occluded = false;
        game.rules.cardBanned(card, this);
        this.repositionCards();
    }

    /**
       If the action was the client's and it was undone
    */
    undoPlayCard(card: Card){

        let index = this.ownCards.indexOf(card);

        if(index > -1){

            this.ownCards.splice(index, 1);
            
        } else {

            index = this.opponentsCards.indexOf(card);

            if(index < 0)
                throw "undoPlayCard didn't find the card";

            this.opponentsCards.splice(index, 1);
        }

        card.occluded = false;
        game.rules.undoCardEnterPlay(card, this);
        this.repositionCards();
    }

    /**
       Calculates positions for all the cards on the field and animates them to their homes
    */
    repositionCards(){

        let posx = 0;
        let posy = this.pixelRect.height - config.fieldCardHeight - 3;
        
        for(let element of this.ownCards){

            // Check third row //
            if(posy < this.pixelRect.height - (config.fieldCardWidth * 4)){

                // Cannot fit //
                element.occluded = true;
                
            } else {

                let pos = this.relativeToAbsolute(new Pos2(posx, posy));

                element.occluded = false;

                element.setHome(pos);
                element.moveHome(500, true);
            }

            // Move to next position //
            posx += config.fieldCardWidth + 2;
            
            if(posx + config.fieldCardWidth >= this.pixelRect.width){

                // Next row //
                posy -= 2 + config.fieldCardHeight;
                posx = 2;
            }
        }

        posx = 0;
        posy = 0;

        for(let element of this.opponentsCards){

            // Check third row //
            if(posy > config.fieldCardWidth * 2){

                // Cannot fit //
                element.occluded = true;
                
            } else {

                let pos = this.relativeToAbsolute(new Pos2(posx, posy));

                element.occluded = false;

                element.setHome(pos);
                element.moveHome(500, false);
            }

            // Move to next position //
            posx += config.fieldCardWidth + 2;
            
            if(posx + config.fieldCardWidth >= this.pixelRect.width){

                // Next row //
                posy += 2 + config.fieldCardHeight;
                posx = 2;
            }
        }
    }

    draw(ctx: CanvasRenderingContext2D, delta: number): boolean{

        ctx.fillStyle = resourcemanager.fpBackground;
        ctx.fillRect(this.pixelRect.x, this.pixelRect.y,
                     this.pixelRect.width, this.pixelRect.height);

        // Border //
        if(!this.canDrop){
            
            drawOutline(ctx, this.pixelRect, "black", false);
            
        } else {

            drawOutline(ctx, this.pixelRect, "gold", false);
        }

        return false;
    }

    onResolutionUpdated(){

        super.onResolutionUpdated();

        // This can be called before the cards have been created so this check is needed
        if(this.ownCards)
            this.repositionCards();
    }

    /**
       Returns true and sets highlighted if the card can be played
    */
    notifyCardOver(point: Pos2, card: Card): boolean{

        if(point == null || card == null){

            this.canDrop = false;
            return false;
        }
        
        if(!this.isPointInside(point) || !game.rules.canPlayCard(card, this)){

            this.canDrop = false;
            return false;
        }

        this.canDrop = true;
        return true;
    }

    

    // TODO: move these to rules
    ownTurnStarted(){

        for(let card of this.ownCards){

            card.attackedThisTurn = false;
        }
    }

    opponentsTurnStarted(){

        for(let card of this.opponentsCards){

            card.attackedThisTurn = false;
        }
    }
}



