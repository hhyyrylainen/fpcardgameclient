
export class Pos2{

    constructor(public x: number, public y: number){

    }

    equals(o: Pos2 | Rect): boolean{

        return this.x == o.x && this.y == o.y;
    }

    /**
       Returns a position between this and target. Progress is between 0.0-1.0
    */
    interpolate(target: Pos2, progress: number): Pos2{

        return new Pos2(this.x + ((target.x - this.x) * progress),
                        this.y + ((target.y - this.y) * progress));
                                  
    }

    /**
       Returns distance to another point
    */
    distance(target: Pos2): number{

        return Math.abs(this.x - target.x) +
            Math.abs(this.y - target.y);
    }

}

export class Size2{
    
    constructor(public width: number, public height: number){

    }
}

export class Rect{

    constructor(public x: number, public y: number,
                public width: number, public height: number)
    {
    }

    fromPos2(data: Pos2): Rect{

        return new Rect(data.x, data.y, 0, 0);
    }

    getPos2(): Pos2{

        return new Pos2(this.x, this.y);
    }

    setPos2(pos: Pos2){

        this.x = pos.x;
        this.y = pos.y;
    }

    getCenter(): Pos2{

        return new Pos2(this.x + (this.width / 2),
                        this.y + (this.height / 2));
    }
}

/**
   Things that might need for some condition to turn true before applying.
   These will be checked each frame so don't spam these too much
   */
export interface DelayApply{

    canApply(): boolean;
    apply(): void;
}

export interface ImageWithSize{

    image: HTMLImageElement,
    width: number,
    height: number
}



