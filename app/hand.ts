import config from "./config";
import { Renderable, drawOutline } from "./drawable";
import { Card } from "./card";
import { Rect, Pos2 } from "./types";


export class Hand extends Renderable{

    // TODO: move this to rules
    private cards: Array<Card> = [];
    

    constructor(public own: boolean){

        super(new Rect(0.3, own ? 0.85 : 0.0, 0.4, 0.15));

    }

    onResolutionUpdated(){

        super.onResolutionUpdated();

        // This can be called before the cards have been created so this check is needed
        if(this.cards)
            this.repositionCards();
    }

    draw(ctx: CanvasRenderingContext2D, delta: number): boolean{
        
        ctx.fillStyle = config.gray;
        ctx.fillRect(this.pixelRect.x, this.pixelRect.y,
                     this.pixelRect.width, this.pixelRect.height);

        // Border //
        drawOutline(ctx, this.pixelRect, config.FpRed, false);

        return false;
    }

    /**
       Adds a new card to this hand, repositions all current and the new card
    */
    addCard(cardObj: Card){

        this.cards.push(cardObj);
        this.repositionCards();
    }

    /**
       Removes a card from this hand, returns true if removed. False if didn't contain the card
    */
    removeCard(matchcard: Card){

        let index = this.cards.indexOf(matchcard);

        if(index < 0)
            return false;

        this.cards.splice(index, 1);
        this.repositionCards();
        
        return true;
    }


    // Sets the home positions of cards in this hand and makes sure that they either snap
    // or animate to the new positions
    repositionCards(){

        if(this.cards.length < 1)
            return;

        // Check is squeezing required //
        let squeeze = 0;

        // Increase squeeze while things don't fit //
        while(this.pixelRect.width - 5 < this.totalWidthWithSqueeze(squeeze)){
            
            squeeze += 5;
        }

        // TODO: modify card indexes to make them fit nicely

        let index = 0;
        for(let card of this.cards){

            card.setHome(this.calculatePositionAtIndex(index, squeeze));

            card.moveHome(150, false);
            ++index;
        }
    }

    /**
       Calculates total width required for all cards with the specified squeeze value
    */
    totalWidthWithSqueeze(squeeze: number): number{

        return (config.HAND_MARGIN * 2) +
            this.cards.length * (config.HAND_PADDING + config.cardWidth - squeeze);
    }

    calculatePositionAtIndex(index: number, squeeze: number): Pos2{

        return new Pos2(this.pixelRect.x + config.HAND_MARGIN +
                ((config.HAND_PADDING + config.cardWidth - squeeze) * index),
                       this.pixelRect.y + config.HAND_MARGIN);
    }
}

