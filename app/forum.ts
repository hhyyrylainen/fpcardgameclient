
import config from "./config";
import resourcemanager from "./resourcemanager";
import { Rect, Pos2 } from "./types";
import { isPointInside } from "./common";
import { Renderable, drawOutline, FontDef } from "./drawable";
import { WrappedText } from "./wrappedtext";
import { Game, game } from "./gamemain";
import { DoPostAction } from "./actions";

abstract class ThingWithTextObj{

    protected textObject: WrappedText;

    public set text(val: string){

        this._text = val;
        this.updateText();
    }

    public set font(val: FontDef){

        this.textObject.font = val;
    }

    constructor(protected _text: string){

        let size = config.font(16);
        this.textObject = new WrappedText(
            this._text, 0, new Pos2(0, 0),
            new FontDef(size,
                        (ctx: CanvasRenderingContext2D) => {
                            ctx.textAlign = "left";
                            ctx.textBaseline = "top";
                            ctx.font = "bold " + size +
                                "px Arial";
                            ctx.fillStyle = "black";
                        }));

    }

    setWidth(width: number){
        
        this.textObject.wrapWidth = width;
    }

    setPos(pos: Pos2){
        this.textObject.pos = pos;
    }

    draw(ctx: CanvasRenderingContext2D, delta: number): boolean{

        this.textObject.draw(ctx, delta);
        return false;
    }

    calculateLines(ctx: CanvasRenderingContext2D){

        this.textObject.calculateLines(ctx);
    }
    
    getRect(ctx: CanvasRenderingContext2D): Rect{

        this.textObject.validate(ctx);
        return this.getRectIfNotDirty();
    }

    getRectIfNotDirty(): Rect{

        if(this.textObject.needsValidation())
            return null;
        
        return new Rect(this.textObject.pos.x, this.textObject.pos.y,
                        this.textObject.wrapWidth, this.textObject.height);
    }

    abstract updateText(): void;
}

/**
   A container for single choice options
*/
export class Choice extends ThingWithTextObj{

    isHighlighted: boolean = false;

    
    constructor(text: string, 
                public effect: string, public id: number,
                public decay: number, public createdTurn: number)
    {
        super(text);
        this.updateText();
    }

    updateText() {
        this.textObject.text = this._text + " ( " + this.effect + " ) " + (
            this.decay > 1 ? this.decay + " Turns Left" : "1 Turn Left");
    }
}

/**
   A single post that has been done
*/
class DonePost extends ThingWithTextObj{

    constructor(text: string, private effectText: string, private poster: string,
                private link: string)
    {
        super(text);
        this.updateText();
    }

    updateText() {
        
        this.textObject.text = this.poster + ": " + this._text;
    }
}



/**
   Contains 3 choices for posts
*/
class PostSelector extends Renderable{

    /**
       If true needs to recalculate text widths
    */
    isDirty = true
    
    choices: Array<Choice> = [];

    protected xOffset = 5;

    public set font(val: FontDef){

        for(let choice of this.choices)
            choice.font = val;
    }

    public get height(){

        return this.pixelRect.height;
    }

    
    constructor(private gameObj: Game) {

        super(new Rect(0, 0, 0, 0));
    }

    updateSize(ctx: CanvasRenderingContext2D, width: number){
        this.isDirty = false

        // Update widths
        if(width != this.pixelRect.width){
            this.pixelRect.width = width

            for(let choice of this.choices){
                choice.setWidth(this.pixelRect.width - 7);
            }
        }
        
        this.pixelRect.height = 0;
        
        for(let choice of this.choices){
            
            choice.calculateLines(ctx);

            this.pixelRect.height += 5 + choice.getRect(ctx).height;
        }

        // Reposition text objects //
        this.position(this.pixelRect.getPos2(), ctx);
    }

    onResolutionUpdated(){

        if(!this.choices)
            return;

        let size = config.font(18);
        this.font = new FontDef(
            size,
            (ctx: CanvasRenderingContext2D) => {
                ctx.textAlign = "left";
                ctx.textBaseline = "top";
                ctx.font = "bold " + size +
                    "px Arial";
                ctx.fillStyle = "black";
            });
    }
    

    position(pos: Pos2, ctx: CanvasRenderingContext2D){

        this.pixelRect.setPos2(pos);

        let yOffset = 5;


        // Calculate positions for the texts
        for(let choice of this.choices){
            choice.setPos(new Pos2(this.pixelRect.x + this.xOffset,
                                   this.pixelRect.y + yOffset));
            yOffset += 5 + choice.getRect(ctx).height;
        }
    }

    setChoice(index: number, choice: Choice): boolean{
        
        if(index < 0 || index > 2){
            return false;
        }
        
        this.isDirty = true;
        
        choice.setWidth(this.pixelRect.width - 7);
        
        this.choices[index] = choice;
        return true;
    }

    getChoice(index: number): Choice{

        if(index < 0 || index > 2){
            return null;
        }
        
        return this.choices[index];
    }

    addChoice(choice: Choice){
        this.isDirty = true;
        choice.setWidth(this.pixelRect.width - 7);
        this.choices.push(choice);
    }

    removeChoice(index: number) {
        this.isDirty = true;
        this.choices.splice(index, 1);
    }

    mouseMove(point: Pos2, ownTurn: boolean){

        for(let choice of this.choices){
            
            choice.isHighlighted = false;
        }

        if(!ownTurn){

            return;
        }
        
        // Update highlighted item
        if(!this.isPointInside(point))
            return;
        
        for(let choice of this.choices){

            let rect = choice.getRectIfNotDirty();

            if(rect && isPointInside(point, rect)){
                choice.isHighlighted = true;
                break;
            }
        }
    }
        
    clickCheck(point: Pos2): boolean {
        if(!this.isPointInside(point))
            return false;

        for(let choice of this.choices){

            let rect = choice.getRectIfNotDirty();
            
            if(isPointInside(point, rect)){
                
                // Post the thing
                game.rules.doClientAction(new DoPostAction(true, choice));
                
                return true;
            }
        }

        return false;
    }
    
    draw(ctx: CanvasRenderingContext2D, delta: number): boolean{

        if(this.choices.length < 1)
            return false;

        if(this.isDirty){
            this.updateSize(ctx, this.pixelRect.width);
        }
        
        // Draw options
        let alternateColour = false;
        
        for(let choice of this.choices){
            
            alternateColour = !alternateColour;
            
            // Backgroud
            if(choice.isHighlighted){
                ctx.fillStyle = config.FpRed
            } else {
                ctx.fillStyle = (alternateColour ? config.gray : "white");
            }

            let rect = choice.getRect(ctx);

            ctx.fillRect(rect.x - this.xOffset, rect.y - 4, this.pixelRect.width,
                         5 + rect.height);
            
            ctx.fillStyle = "black";
            choice.draw(ctx, delta);
        }

        // Draw box
        drawOutline(ctx, this.getRect(), "black", false);
        return false;
    }
}



// Container for all the posts
class PostContainer extends Renderable{

    forcedUpdate: boolean = true;

    posts: Array<DonePost> = [];

    xPadding = 15;
    
    constructor(private maxPosts: number = 20){
        
        super(new Rect(0, 0, 0, 0));

        this.onResolutionUpdated();
    }

    onResolutionUpdated(){
        
        // Skip if called from super constructor
        if(!this.posts)
            return;

        for(let post of this.posts){
            
            let size = config.font(16);
            
            post.font = new FontDef(
                size,
                (ctx: CanvasRenderingContext2D) => {
                    ctx.textAlign = "left";
                    ctx.textBaseline = "top";
                    ctx.font = "bold " + size +
                        "px Arial,Tahoma,Calibri,Verdana,Geneva,sans-serif";
                    ctx.fillStyle = "black";
                });
        }
    }
    
    draw(ctx: CanvasRenderingContext2D, delta: number): boolean{

        ctx.save();
        
        if(this.forcedUpdate){

            this.calculatePositions(ctx);
        }

        // Setup clipping
        ctx.moveTo(this.pixelRect.x, this.pixelRect.y);
        ctx.lineTo(this.pixelRect.x + this.pixelRect.width, this.pixelRect.y);
        ctx.lineTo(this.pixelRect.x + this.pixelRect.width,
                   this.pixelRect.y + this.pixelRect.height);
        ctx.lineTo(this.pixelRect.x, this.pixelRect.y + this.pixelRect.height);
        ctx.lineTo(this.pixelRect.x, this.pixelRect.y);
        ctx.clip();
        
        let alternateColour = false;
        
        // Posts
        for(let post of this.posts){
            
            alternateColour = !alternateColour
            
            // Background colour
            if(alternateColour){
                ctx.fillStyle = "white";
            } else {
                ctx.fillStyle = config.FpGreen;
            }

            let postRect = post.getRect(ctx);

            ctx.fillRect(this.pixelRect.x, postRect.y, this.pixelRect.width, postRect.height);
            
            // Post text
            post.draw(ctx, delta);
        }

        // Outline
        drawOutline(ctx, this.getRect(), "black", false);

        ctx.restore();
        return false;
    }

    addPost(post: DonePost) {
        this.forcedUpdate = true;
        this.posts.push(post);

        while(this.posts.length > this.maxPosts){
            this.posts.shift();
        }
    }   

    setNewSize(rect: Rect){

        this.pixelRect = rect;
        this.forcedUpdate = true;
    }
    
    calculatePositions(ctx: CanvasRenderingContext2D) {
        
        this.forcedUpdate = false;

        for(let post of this.posts){

            post.setWidth(this.pixelRect.width - (this.xPadding / 2) - 4);
        }

        // Position the posts
        let yOffset = this.pixelRect.height - 2;

        for(let i = this.posts.length - 1; i >= 0; --i){
            
            let post = this.posts[i];
            
            let postRect = post.getRect(ctx);
            
            post.setPos(new Pos2(this.pixelRect.x + (this.xPadding / 2),
                                 this.pixelRect.y + yOffset - postRect.height));
            yOffset -= postRect.height + 2;

            if(yOffset < 0)
                break;
        }
    }
}


export class Forum extends Renderable{

    titleText: WrappedText;
    postSelector: PostSelector;
    postContainer: PostContainer;

    /**
       True when positions need to be recalculated for things
    */
    isDirty: boolean = true;
    
    constructor() {

        super(new Rect(0.0015, 0.16, 0.29, 0.837));

        
        this.titleText = new WrappedText(
            "Thread", 0, new Pos2(0, 0),
            null);
        
        this.postSelector = new PostSelector(game);
        this.postContainer = new PostContainer(15);

        this.onResolutionUpdated();
    }

    onResolutionUpdated(){
        
        super.onResolutionUpdated();
        this.isDirty = true;

        // Called from super constructor
        if(!this.postSelector)
            return;

        
        this.postSelector.onResolutionUpdated();
        this.postContainer.onResolutionUpdated();

        if(!this.titleText.font || this.titleText.font.fontSize != config.font(24)){

            let size = config.font(24);
            this.titleText.font = new FontDef(
                size,
                (ctx: CanvasRenderingContext2D) => {
                    ctx.textAlign = "left";
                    ctx.textBaseline = "top";
                    ctx.font = "bold " + size +
                        "px Arial,Tahoma,Calibri,Verdana,Geneva,sans-serif";
                    ctx.fillStyle = "white";
                });
        }
    }

    canClickStuff(): boolean{

        return game.rules.ownTurn && !game.rules.postedThisTurn && !game.selectingTarget;
    }
    
    mouseMove(point: Pos2): boolean{

        this.postSelector.mouseMove(point, this.canClickStuff());
        return false;
    }
    
    clickCheck(point: Pos2): boolean{

        if(!this.canClickStuff())
            return false;
        
        return this.postSelector.clickCheck(point);
    }
    
    updateThread(title: string, link: string) {
        this.titleText.text = title;
        this.isDirty = true;
    }
    
    addPost(message: string, effect: string, matchid: number, decay: number,
            turnNumber: number)
    {
        this.postSelector.addChoice(new Choice(message, effect, matchid,
                                               decay, turnNumber));
        this.isDirty = true;
    }

    /**
       When we mispredict that the server allows us to make a post the post is returned
       through this method
    */
    returnFailedPost(post: Choice){
        
        this.postSelector.addChoice(post);
        this.isDirty = true;
    }
    
    decayPost(matchid: number){
        
        for(let i = 0; i < this.postSelector.choices.length; ++i){

            let choice = this.postSelector.choices[i];
            
            if(choice.id == matchid){
                this.postSelector.removeChoice(i);
                this.isDirty = true;
                return;
            }
        }
    }

    /**
       Updates decay variables for all the posts
    */
    updateDecay(turnNumber: number) {

        for(let choice of this.postSelector.choices){

            if(choice.createdTurn == turnNumber)
                continue;
            
            choice.decay = choice.decay - 1;
            choice.updateText();
        }
    }

    newPostDone(message: string, effect: string, poster: string, link: string) {

        this.postContainer.addPost(new DonePost(message, effect, poster, link));
    }

    /**
       Repositions all sub components
    */
    rePositionComponents(ctx: CanvasRenderingContext2D){
        
        this.titleText.wrapWidth = this.pixelRect.width - 5;
        this.titleText.pos = new Pos2(this.pixelRect.x + 6, this.pixelRect.y + 12);
        

        this.postSelector.updateSize(ctx, this.pixelRect.width - 3);

        let selectorPos = new Pos2(this.pixelRect.x + 2,
                                   (this.pixelRect.y + this.pixelRect.height) -
                                   (this.postSelector.height));
        
        this.postSelector.position(selectorPos, ctx);



        this.titleText.validate(ctx);
        let startY = this.titleText.pos.y + this.titleText.height + 5;
        
        this.postContainer.setNewSize(new Rect(
            this.pixelRect.x, startY,
            this.pixelRect.width, (selectorPos.y - 5) - startY));

        this.isDirty = false;
    }
    
    draw(ctx: CanvasRenderingContext2D, delta: number): boolean{
        
        if(this.isDirty){
            
            this.rePositionComponents(ctx);
        }

        // Background
        ctx.fillStyle = resourcemanager.fpBackground;
        ctx.fillRect(this.pixelRect.x, this.pixelRect.y,
                     this.pixelRect.width, this.pixelRect.height);


        // Render thread title
        // A nice blue background
        ctx.fillStyle = config.FpBlue;

        this.titleText.validate(ctx);
        let titleLineEndY = this.titleText.pos.y + this.titleText.height + 5;
        ctx.fillRect(this.pixelRect.x, this.pixelRect.y,
                     this.pixelRect.width, 1 + titleLineEndY - this.pixelRect.y);

        this.titleText.draw(ctx, delta);



        // Ending line
        ctx.fillStyle = "black";
        ctx.lineWidth = 4;
        
        ctx.beginPath();
        
        ctx.moveTo(this.pixelRect.x + 1, titleLineEndY);
        ctx.lineTo(this.pixelRect.x + this.pixelRect.width - 1, titleLineEndY);
        ctx.stroke();

        // Choices
        this.postSelector.draw(ctx, delta);

        // Posts
        this.postContainer.draw(ctx, delta);
        
        drawOutline(ctx, this.pixelRect, "black", false);

        return false;
    }
}


