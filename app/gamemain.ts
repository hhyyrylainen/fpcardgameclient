import * as $ from 'jquery';


import {} from "./common";
import config from "./config";
import resourcemanager from "./resourcemanager";

import { Pos2, Rect } from "./types";
import { Card, cardholder, CardAnimationAttact } from "./card";

import { Networking } from "./networking";
import { Rules } from "./gamerules";
import { Renderable, ClickableThing } from "./drawable";


import { ChatMessageMessage, StartRequestMessage, ClientChallengeRequest,
         DiscardCardEffectMessage, EndTurnRequestMessage, ClientRequestDefeatMessage
       } from "./messages";

import { SelectTargetAction, AttackAction, PlayCardAction } from "./actions";




import { PlayerInfoBox } from "./playerbox";
import { Hand } from "./hand";
import { Battlefield } from "./battlefield";
import { BanCamp } from "./bancamp";
import { Button } from "./button";
import { Forum } from "./forum";
import { WinScreen } from "./winscreen";
import { SpellEffect } from "./spelleffect";
import { TimedText } from "./timedtext";
import { FloatingNumber } from "./floatingnumber";
import { voidCard } from "./void";

import "tinysort";

enum GAME_STATE {

    Loading,

    /**
       The main lobby screen
    */
    Main,
    Lobby,

    /**
       The main gameplay screen
    */
    Match
}

enum DRAG_TARGET {

    None,
    AttackCard,
    PlayCard
}

class OnlinePlayer{

    constructor(public name: string,
                public challenge: boolean = true)
    {
    }
}

export class Game{

    /**
       Current main state
    */
    State: GAME_STATE = GAME_STATE.Loading;

    // Main components //
    networking: Networking = null;

    rules: Rules = null;

    canvas: HTMLCanvasElement = null;
    ctx: CanvasRenderingContext2D = null;

    width: number;
    height: number;

    /**
       Current mouse position, updated in mouse move handler
    */
    mousePos: Pos2 = new Pos2(0, 0);

    /**
       True when left mouse button is down
    */
    mouseLeftDown: boolean = false;

    /**
       Position where the current mouse pres started
    */
    mouseDownStart: Pos2 = new Pos2(0, 0);

    /**
       Timestamp when the match draw function was last called
    */
    lastGameDraw: number = null;

    /**
       True when canvas size has changed and objects need to be adjusted
    */
    needsResize: boolean = false;


    // Challenge stuff and autostart //


    wantsToPlayWith: string = null;


    doingChallengeRequest: boolean = false;
    challengeTimeOutStart: Date = null;

    /**
       Timer for checking timeouts when challenging players
    */
    challengeTimeOutUpdate: number = null;
    

    /**
       Valid when rules.matchShowScores == true
    */
    matchScoresWindow: WinScreen = null;

    // Selecting target card //
    /**
       True when the server has told us to select a target
    */
    selectingTarget: boolean = false;

    /**
       What the server wants us to select
    */
    selectTargetString: string;

    /**
       True when selectingTarget and the user is on a valid target
    */
    selectValidTarget: boolean = false;

    /**
       Valid when selectValidtarget and a card was the wanted target
    */
    selectTargetCard: Card = null;

    // Select restrictions, used to provide user with feedback while selecting //
    selectTargetsCard: boolean = false;
    selectTargetsOwn: boolean = false;
    selectTargetsOpponents: boolean = false;
    

    /**
       Position of cursor, updated when selectingTarget
    */
    selectCurrentPosition: Pos2 = new Pos2(0, 0);

    /**
       Card for which the big popup card is shown
    */
    hoveredCard: Card = null;
    
    // Drag attack target //
    /**
       True if doing dragging right now
    */
    isDragging: boolean = false;
    
    /**
       Drag target
     */
    dragTarget: DRAG_TARGET = DRAG_TARGET.None;

    /**
       If dragTarget == DRAG_TARGET.AttackCard then this is the target
    */
    dragAttackTarget: Card = null;

    /**
       Card that the user has grabbed and is shaking around
    */
    draggedCard: Card = null;
    

    /**
       Opponent's name in a match
    */
    opponent: string = "";

    /**
       Own name
    */
    playerName: string = "";

    /**
       List of players in lobby
    */
    onlinePlayerList: Array<OnlinePlayer> = [];


    // Game objects //
    // Used when State == GAME_STATE.Match

    /**
       All the GUI controls and backgrounds and stuff
    */
    uiElements: Array<Renderable> = [];

    // Access to specific UI components //
    opponentInfoBox: PlayerInfoBox;
    ownHand: Hand = null;
    opponentsHand: Hand = null;
    mainBattlefield: Battlefield = null;
    banCamp: BanCamp = null;
    forum: Forum = null;
    turnEndButton: Button = null;
    giveUpButton: Button = null;

    /**
       GUI controls that respond to mouse events
    */
    uiClickable: Array<ClickableThing> = [];
    

    /**
       Fake card shown in menu background
    */
    exampleCard: Card = null;

    // Init functions //
    /**
       Called (by application.ts) when the user wants to start the game
    */
    init(){

        this.State = GAME_STATE.Loading;

        // Open connection now to get it ready as soon as possible //
        this.networking = new Networking();
        
        this.canvas = document.getElementById("GameRender") as HTMLCanvasElement;
        this.ctx = this.canvas.getContext("2d");

        this.setupjQueryThings();

        // Create an example card that is shown in the background
        this.exampleCard = cardholder.receiveCard(
            {"definition":
             {"attack":3,"defence":12, "cost": 6,
              "description":"The guy whose birthday is every couple of years, "+
              "also made a mod for some game",
              "id":1, type: "user",
              "image":"\/images\/cards\/Garry.png","name":"Garry"},
             "full":true,"hidden":false,"id":1,"matchcard":1});

        if(!this.exampleCard)
            throw "failed to create example card";

        // Make it huge //
        this.exampleCard.scale = 3.5;
        this.exampleCard.setHome(new Pos2(50, 300));
        this.exampleCard.setToHome();

        // Play attack animation
        //this.exampleCard.setAnimation(new CardAnimationAttact(new Pos2(600, 250), 2000, 1000));
    }

    /**
       Registers jQuery things the game needs
    */
    private setupjQueryThings(){

        $(window).on('resize', () => {

            this.updateCanvas();
            // Redraw on next frame //
            window.requestAnimationFrame(this.draw.bind(this));
        });

        // Init game buttons //
        $("#PlayEasyButton").button().click( (event: JQueryEventObject) => {
            
            event.preventDefault();
            this.playEasyMatch();
        });

        // Chat message //
        $("#ChatMessageSubmit").button().click( (event: JQueryEventObject) => {
            
            event.preventDefault();
            let sendstr = $("#ChatMessageEnter").val();
            
            if(sendstr.length < 1)
                return;
            
            console.log("Sending chat: " + sendstr);
            $("#ChatMessageEnter").val("");

            this.networking.send(new ChatMessageMessage(sendstr, ""));
        });

        // Limit input length
        $("#ChatMessageEnter").on('input', (e: JQueryInputEventObject) => {

            let text = $(e.srcElement).val();

            if(!text)
                return;
            
            let length = text.length;
            if(length > 400){

                $(this).val(text.substring(0, 400));
            }
        });

        $("#ChatMessageEnter").keyup((e: JQueryKeyEventObject) => {
            
            if(e.keyCode == 13){
                
                $("#ChatMessageSubmit").click();
            }
        });
    }

    /**
       Called after game is ready (from networking), hides unnecessary things
    */
    postInit(){

        this.updateCanvas();

        // Create canvas resources //
        resourcemanager.createResources(this.ctx);

        $("#LoadingIndicator").hide();
        
        console.log("Started the Game!");
        
        this.clearScreen();

        this.setGameState(GAME_STATE.Main);

        // Check for a queued match //
        if(this.wantsToPlayWith != null){

            console.log("Queued match with " + this.wantsToPlayWith);

            this.doStartMatch(this.wantsToPlayWith);
            this.wantsToPlayWith = null;
        }
    }

    /**
       Confirmed doStartMatch

       This is the main setup function for all gameplay related objects
    */
    onConfirmMatch(opponentName: string){

        // Set / reset match variables //
        this.rules = new Rules();
        
        this.opponent = opponentName;

        // Create UI components and stuff
        this.uiElements = [];
        this.uiClickable = [];

        // Opponent info box
        this.opponentInfoBox = new PlayerInfoBox(this.opponent,
                                                 0, 0, 0, 0);

        this.uiElements.push(this.opponentInfoBox);
        
        
        // Create hands //
        this.ownHand = new Hand(true);
        this.uiElements.push(this.ownHand);
        this.opponentsHand = new Hand(false);
        this.uiElements.push(this.opponentsHand);

        // Create game battlefield //
        this.mainBattlefield = new Battlefield();
        this.uiElements.push(this.mainBattlefield);


        // Create bancamp //
        this.banCamp = new BanCamp();
        this.uiElements.push(this.banCamp);
        
        // Create end turn button //
        this.turnEndButton = new Button(new Rect(0.75, 0.92, 0.05, 0.07), "End Turn",
                                        (btn: Button) => {
                                            this.pressedEndTurn();
                                        }, config.FpRed, "green", "black", "black",
                                        "14px Times New Roman");
        
        this.turnEndButton.enabled = false;
        this.uiElements.push(this.turnEndButton);
        this.uiClickable.push(this.turnEndButton);
        

        // Create give up button
        this.giveUpButton = new Button(new Rect(0.81, 0.92, 0.05, 0.07), "Give Up",
                                       (btn: Button) => {
                                           this.pressedGiveUp();
                                       }, "white", config.FpRed, "black", "black",
                                       "14px Times New Roman");
        
        this.giveUpButton.enabled = false;
        this.uiElements.push(this.giveUpButton);
        this.uiClickable.push(this.giveUpButton);

        // Create forum object //
        this.forum = new Forum();
        this.uiElements.push(this.forum);
        this.uiClickable.push(this.forum);

        // Move to match state //
        this.setGameState(GAME_STATE.Match);
    }


    updateCanvas(){

        this.needsResize = true;
        config.updateGameWidth( $("#GameBox").width(), $("#GameBox").height());
    }

    // End of init stuff

    doErrorRedirect(error: string){

        window.location.href = "/index.html?errormessage=" + error;
    }

    /**
       Hides / Shows game elements based on state
    */
    setGameState(newstate: GAME_STATE){

        if(this.State == newstate)
            return;

        // State exit code //
        switch(this.State){
        case GAME_STATE.Main:
            // Hide menu things //
            $("#GameButtons").hide();
            break;
        case GAME_STATE.Match:

            // Clear user controls //
            this.resetDragAndHover();

            // Destroy all gameplay objects
            this.rules = null;
            break;
        }

        this.State = newstate;

        // State enter code //

        // Show things //
        switch(this.State){
        case GAME_STATE.Main:
            // Show menu things //
            $("#GameButtons").show();

            break;
        }
        
        // Should do atleast one repaint //
        window.requestAnimationFrame(this.draw.bind(this));
    }

    //
    // Drawing stuff
    //
    clearScreen(){

        this.ctx.fillStyle = "#FFFFFF";
        this.ctx.fillRect(0, 0, this.width, this.height); 
    }
    
    
    /**
       Draws on to the canvas according to current state
    */
    draw(timestamp: number){

        this.doActualResize();
        this.clearScreen();

        switch(this.State){
        case GAME_STATE.Lobby:

            this.ctx.textAlign = "center";
            
            this.ctx.save();
            
            this.ctx.fillStyle = config.FpRed;

            this.ctx.shadowOffsetX = 2;
            this.ctx.shadowOffsetY = 2;
            this.ctx.shadowBlur = 2;
            this.ctx.shadowColor = "rgba(0, 0, 0, 0.5)";
            
            this.ctx.font = "42px Times New Roman";
            this.ctx.fillText("Waiting for Connection...", this.width / 2,
                              this.height / 6);

            this.ctx.restore();

            this.ctx.fillStyle = "black";
            
            this.ctx.font = "36px Times New Roman";
            this.ctx.fillText(this.opponent, this.width / 2, this.height / 2);

            this.ctx.fillStyle = "rgba(30, 30, 30, 0.7)";
            
            this.ctx.font = "18px Times New Roman";
            this.ctx.fillText("Your Opponent", this.width / 2, this.height / 2 + 50);

            break;
            
        case GAME_STATE.Match:

            // Switch to game draw function //
            this.lastGameDraw = timestamp;
            this.gameDraw(timestamp);
            return;
            
        case GAME_STATE.Main:

            // Background //
            this.ctx.fillStyle = resourcemanager.fpBackground;
            
            this.ctx.fillRect(0, 0, config.width, config.height);


            // Test shield //
            this.ctx.drawImage(resourcemanager.activeSword, 25, 25, 125, 125);

            this.ctx.drawImage(resourcemanager.normalShield, 200, 25, 125, 125);

            // Example card //
            this.exampleCard.draw(this.ctx, 16);
            
            if(!this.exampleCard.typeInfo.imageLoaded){

                // Redraw until the card is loaded //
                window.requestAnimationFrame(this.draw.bind(this));
                
            }
            break;
        }
    }

    /**
       Draws the game status.
       this.State == StateEnum.MATCH must be true
    */
    gameDraw(timestamp: number){

        if(this.State != GAME_STATE.Match)
            return;

        if(!this.lastGameDraw)
            this.lastGameDraw = timestamp - 1;

        // Amount of milliseconds passed since last draw //
        let delta = timestamp - this.lastGameDraw;
        this.lastGameDraw = timestamp;

        if(isNaN(delta))
            delta = 16;

        // Handle delayed things
        this.rules.executeQueue();
        
        this.clearScreen();
        this.doActualResize();

        // Draw UI stuff like the backgrounds and hand containers etc. //
        for(let drawable of this.uiElements){

            this.ctx.save();

            let res = drawable.draw(this.ctx, delta);
            
            this.ctx.restore();

            if(res === true)
                throw "uiElements drawable wants to disappear after .draw()";
        }

        // Draw all cards //
        this.rules.drawStateEarly(this.ctx, delta);
        
        // Attack arrow //
        if(this.dragTarget == DRAG_TARGET.AttackCard){

            this.ctx.save();
            
            if(!this.dragAttackTarget)
                throw "not good, this.dragAttackTarget";

            if(!this.draggedCard)
                throw "not good, this.dragAttackTarget";

            this.ctx.beginPath();
            this.ctx.strokeStyle = config.FpRed;
            this.ctx.lineWidth = 4;

            let origin = this.draggedCard.getRect(true).getCenter();
            
            this.ctx.moveTo(origin.x, origin.y);

            let target = this.dragAttackTarget.getRect().getCenter();
            let pos = this.draggedCard.getRect();
            this.ctx.quadraticCurveTo(pos.x, pos.y,
                                      target.x, target.y);
            
            this.ctx.stroke();

            this.ctx.restore();
        }

        this.rules.drawStateLate(this.ctx, delta);

        if(this.rules.matchShowScores){
            
            this.ctx.save();
            this.matchScoresWindow.draw(this.ctx, delta);
            this.ctx.restore();
        }

        // Selecting view //
        if(this.selectingTarget){

            this.drawTargetSelect();
        }
        
        // Draw again //
        window.requestAnimationFrame(this.gameDraw.bind(this));
    }

    /**
       Draws all the ui related to selecting a target
       TODO: make this into a class
    */
    drawTargetSelect(){

        this.ctx.save();
        // Targeting arrow //
        this.ctx.beginPath();

        this.ctx.moveTo(config.width / 2, 15);

        let arrowhelper = (pos: Pos2, size: number) => {

            // Left side //
            this.ctx.lineTo(pos.x - ( size / 2 ),
                            pos.y - Math.floor(size * 1.2));
            
            this.ctx.lineTo(pos.x - size,
                            pos.y - Math.floor(size * 1.2));
            // Tip point //
            this.ctx.lineTo(pos.x,
                            pos.y);

            // Right side //
            this.ctx.lineTo(pos.x + size,
                            pos.y - Math.floor(size * 1.2));

            this.ctx.lineTo(pos.x + ( size / 2 ),
                            pos.y - Math.floor(size * 1.2));
        };
        
        if(this.selectValidTarget){

            if(!this.selectTargetCard)
                throw "Game Draw select target is valid, but isn't a card, todo: " +
                "implement this type"
            
            // Draw target arrow to the card //
            this.ctx.fillStyle = "green";

            arrowhelper(this.selectTargetCard.getRect().getCenter(), 15);
            
        } else {
            
            if(this.selectCurrentPosition){

                // Draw target arrow to mouse pos//
                this.ctx.fillStyle = config.FpRed;
                
                arrowhelper(this.selectCurrentPosition, 15);
            }
        }

        this.ctx.closePath();
        this.ctx.fill();
        
        // Draw message //
        this.ctx.fillStyle = config.FpRed;
        let lineHeight = config.font(28);

        this.ctx.fillRect(0, 0, config.width, lineHeight + 10);

        // Text //
        this.ctx.fillStyle = "white";
        this.ctx.font = "bold " + lineHeight + "px Arial";
        this.ctx.textAlign = "center";
        this.ctx.textBaseline = "top";
        
        this.ctx.fillText(this.selectTargetString, config.width / 2, 5);

        // Discard button / text
        this.ctx.textAlign = "left";
        lineHeight = config.font(14);

        this.ctx.font = lineHeight + "px Arial";

        if(this.mousePos.x < config.width / 3 &&
           this.mousePos.y < config.font(28) + 10)
        {
            this.ctx.fillStyle = "white";
            
        } else {
            
            this.ctx.fillStyle = "black";
        }

        this.ctx.fillText("Discard Effect", 10, 8);

        
        this.ctx.restore();        
    }



    /**
       Actually handles resizing things
    */
    doActualResize(){

        if(!this.needsResize)
            return;

        this.needsResize = false;

        this.width = $("#GameBox").width();
        this.height = $("#GameBox").height();

        this.canvas.width = this.width;
        this.canvas.height = this.height;
        //this.ctx.width = this.width;
        //this.ctx.height = this.height;

        config.updateGameWidth(this.width, this.height);

        // Update UI stuff //
        for(let drawable of this.uiElements){

            drawable.onResolutionUpdated();
        }
    }

    //
    // Game state data query
    //
    
    /**
       Gets the player's name
    */
    getPlayerName(){

        return this.playerName;
    }

    /**
       Called when locally changing name
    */
    setPlayerName(newname: string){

        this.playerName = newname;
    }

    /**
       Updates name when the server tells us to
    */
    updateName(newname: string){

        this.playerName = newname;
    }

    getOwnEnergy(): number{

        if(!this.opponentInfoBox)
            return 0;

        return this.opponentInfoBox.ownEnergy;
    }

    getCardAtPoint(point: Pos2): Card{

        let targetcard: Card = null;
        
        for(let element of this.rules.cardsInPlay){

            if(!this.rules.canPointAtCard(element))
                continue;
            
            if(!element.isPointInside(point))
                continue;

            targetcard = element;
            break;
        }

        return targetcard;
    }
    
    //
    // Match startup and requests
    //

    playEasyMatch(){

        if(this.State == GAME_STATE.Main){

            // Lets go now //
            this.doStartMatch(config.AINames.EASY);
            return;
        }

        // Need to queue this //
        this.wantsToPlayWith = config.AINames.EASY;
    }

    
    /**
       Starts playing a match against the opponent
    */
    doStartMatch(opponent: string){

        console.log("Starting a match with " + opponent);
        
        this.opponent = opponent;
        
        this.setGameState(GAME_STATE.Lobby);

        // Send start request //
        this.networking.send(new StartRequestMessage(opponent));
    }

    resetChallengeVariables(){

        clearInterval(this.challengeTimeOutUpdate);
        
        this.doingChallengeRequest = undefined;
        this.challengeTimeOutStart = undefined;
        this.challengeTimeOutUpdate = undefined;

        // Close dialog if open //
        if($("#ChallengeWaitingDialog").dialog("isOpen")){
            
            $( "#ChallengeWaitingDialog" ).dialog("close");
        }
    }

    matchRequestFailed(reason: string){

        if(!reason)
            reason = "Undefined reason";
        
        // Close the dialog //
        this.resetChallengeVariables();

        // Show failed dialog //
        $( "#ChallengeConnectFailedDialog" ).dialog({
            resizable: false,
            height: 240,
            width: 480,
            modal: true,
            title: "Match Request Failed",
            buttons: {
                OK: function() {

                    $( "#ChallengeConnectFailedDialog" ).dialog( "close" );
                }
            }
        }).show();

        $("#ChallengeConnectFailReason").text(reason);
    }

    cancelRequestMatch(opponent: string){

        this.networking.send(new ClientChallengeRequest(false, opponent));

        // Close the dialog //
        this.resetChallengeVariables();
    }

    challengeRequestAccepted(opponent: string){

        try{
            
            this.resetChallengeVariables();
            
        } catch(e){

        }

        console.log("Starting a match after successful challenge with " + opponent);

        this.opponent = opponent;
        
        this.setGameState(GAME_STATE.Lobby);
    }

    doRequestMatch(opponent: string, timeout: number){

        if(this.doingChallengeRequest){

            this.cancelRequestMatch(opponent);
        }

        $( "#ChallengeWaitingDialog" ).dialog({
            resizable: false,
            height: 240,
            width: 480,
            modal: true,
            title: "You Challenged " + opponent,
            close: () => {
                $( "#ChallengeWaitingDialog" ).dialog( "close" );
                this.cancelRequestMatch(opponent);
            },
            buttons: {
                Cancel: () => {
                    this.cancelRequestMatch(opponent);
                }
            }
        }).show();

        $("#WaitingChallengeText").html("Waiting for opponent to accept challenge...<br>"+
                                        "Timeout in 60 seconds");

        this.networking.send(new ClientChallengeRequest(true, opponent));

        // Tell the server we want this stuff //
        this.doingChallengeRequest = true;
        this.challengeTimeOutStart = new Date();
        this.challengeTimeOutUpdate = window.setInterval(() => {

            let secondsleft = 60 - Math.floor((new Date().valueOf() -
                                               this.challengeTimeOutStart.valueOf()) / 1000);

            if(secondsleft < 0){

                this.matchRequestFailed("Opponent didn't accept in time");
                
            } else {

                $("#WaitingChallengeText").html(
                    "Waiting for opponent to accept challenge...<br>"+
                        "Timeout in " + secondsleft + " seconds");
            }
            
        }, 1000);
    }

    receiveMatchRequest(fromply: string){

        let sent = false;

        $( "#ChallengeToAMatchDialog" ).dialog({
            resizable: false,
            height: 240,
            width: 480,
            modal: true,
            title: "You Have Been Challenged",
            close: () => {
                $( "#ChallengeToAMatchDialog" ).dialog( "close" );
                if(!sent)
                    this.networking.send(new ClientChallengeRequest(false, fromply));
            },
            buttons: {
                Deny: () =>  {

                    sent = true;
                    $( "#ChallengeToAMatchDialog" ).dialog( "close" );
                    this.networking.send(new ClientChallengeRequest(false, fromply));
                },
                Accept: () =>  {
                    
                    sent = true;
                    $( "#ChallengeToAMatchDialog" ).dialog( "close" );
                    this.networking.send(new ClientChallengeRequest(true, fromply));
                }
            }
        }).show();
        
        $("#ChallengeToAMatchMessage").text(fromply + " wants to play against you");
    }

    onOtherCancelledMatchRequest(){

        // Don't try to close if it isn't a dialog //
        if (!$("#ChallengeToAMatchDialog").hasClass('ui-dialog-content'))
            return;
        
        $( "#ChallengeToAMatchDialog" ).dialog("close");
    }



    //
    // Message receive functions
    //

    onReceiveDrawnCard(cardobj: Card, opponents: boolean){

        if(opponents){
            
            // Not our card //
            this.opponentsHand.addCard(cardobj);
            
            cardobj.setPos(new Pos2(this.width / 2, -250));
            
        } else {

            // Our card //
            this.ownHand.addCard(cardobj);

            cardobj.setPos(new Pos2(this.width / 2, this.height + 250));
        }

        // Replace automatic animation //
        cardobj.moveHome(230, true);

        this.rules.cardsInPlay.push(cardobj);
    }


    appendPlayerToOnline(name: string, canchallenge: boolean){

        this.onlinePlayerList.push(new OnlinePlayer(name, canchallenge));
    }

    removePlayerFromOnline(name: string){

        this.onlinePlayerList = this.onlinePlayerList.filter(function(value: OnlinePlayer){
            
            return value.name != name;
        });
        
        // TODO: avoid whole rebuild of the list
        this.sortPlayerList();
    }

    notifyAllowChallenge(name: string, canchallenge: boolean){

        for(let ply of this.onlinePlayerList){
            
            if(ply.name == name){

                ply.challenge = canchallenge;
                break;
            }
        }

        // TODO: find a way to update just this one
        this.sortPlayerList();
    }

    /**
       Server sent a list of online players
    */
    notifyOnlinePlayers(playerList: Array<any>){
        
        // Add players //
        for(let ply of playerList){

            this.appendPlayerToOnline(ply["n"], ply["c"]);
        }

        this.sortPlayerList();
    }

    /**
       A single player left or connected
    */
    notifyOnlinePlayer(player: string, isconnected: boolean){

        if(isconnected){

            // Add to the list //
            this.appendPlayerToOnline(player, true);
            this.sortPlayerList();
            
        } else {

            // Remove existing one //
            this.removePlayerFromOnline(player);
            
            //$('#PlayerList li').filter(function() {
            //     return $.text([this]) === player; }).remove();
        }
    }

    notifyEnergy(own: boolean, energy: number){

        if(!this.opponentInfoBox)
            return;
        
        if(own){

            this.opponentInfoBox.ownEnergy = energy;
            
        } else {

            this.opponentInfoBox.opponentsEnergy = energy;
        }
    }

    notifyWinnerRatings(own: boolean, winner: number){

        if(!this.opponentInfoBox)
            return;
        
        if(own){

            this.opponentInfoBox.ownScore = winner;
            
        } else {

            this.opponentInfoBox.opponentsScore = winner;
        }        
    }

    notifyThread(title: string, link: string){

        this.forum.updateThread(title, link);
    }
    
    notifyCanDoPost(message: string, effect: string, matchid: number, decay: number){

        this.forum.addPost(message, effect, matchid, decay, this.rules.turnNumber);
    }

    notifyPostDecayed(matchid: number){

        this.forum.decayPost(matchid);
    }

    notifyNewPost(message: string, effect: string, poster: string, link: string){

        this.forum.newPostDone(message, effect, poster, link); 
    }

    notifyTurn(own: boolean){

        this.rules.onTurnChanged(own);
        
        if(own){

            // Set own cards able to attack again //
            this.mainBattlefield.ownTurnStarted();

            this.rules.timedTexts.push(new TimedText(
                "Your Turn", 3500,
                new Pos2(config.width / 2, 60 + config.height / 2), 58));

            // Update post timeouts //
            this.forum.updateDecay(this.rules.turnNumber);
            
        } else {

            // Reset attacked status on enemy cards //
            this.mainBattlefield.opponentsTurnStarted();

            this.resetDrag();

            this.rules.timedTexts.push(new TimedText(
                "Opponent's Turn", 3500,
                new Pos2(config.width / 2, 60 + config.height / 2), 58));
        }

        this.turnEndButton.enabled = this.rules.ownTurn;
        this.giveUpButton.enabled = true;
    }

    notifyActionFinished(idnumber: number, succeeded: boolean){

        if(succeeded){
            
            this.rules.serverAgreed(idnumber);
            
        } else {

            console.warn("Client action didn't succeed, undoing it");
            this.rules.undoClientAction(idnumber);
        }
    }

    notifyCardBanned(matchid: number, banned: boolean, werescued: boolean){

        let matchcard = this.rules.getCardByMatchID(matchid);

        if(!matchcard || !matchcard.inPlay){

            console.error("notify card banned can't find target card");
            return;
        }

        if(banned){

            // Banned, move to refugee camp //
            this.mainBattlefield.removeCard(matchcard);
            this.banCamp.addCard(matchcard);
            
            
        } else {

            // Unbanned add to battlefield //
            this.banCamp.removeCard(matchcard);
            this.mainBattlefield.addCard(werescued, matchcard);

            // Can't attack just after unbanning
            matchcard.attackedThisTurn = true;
        }
    }

    notifyCardVoided(matchid: number){

        let matchcard = this.rules.getCardByMatchID(matchid);

        if(!matchcard){

            console.error("notify card voided can't find target card");
            return;
        }

        console.log("Card " + matchid + " voided");

        // Remove from field and ban camp //
        try {
            this.banCamp.removeCard(matchcard);
        } catch(e){
            // Ignore
        }

        try {
            this.mainBattlefield.removeCard(matchcard);
        } catch(e){
            // Ignore
        }
        
        // Remove from hands //
        this.ownHand.removeCard(matchcard);
        this.opponentsHand.removeCard(matchcard);

        // Add to void for a cool effect //
        voidCard(matchcard);
    }

    notifyCardStats(matchid: number, attack: number, defence: number){

        // Find target card //
        let matchcard = this.rules.getCardByMatchID(matchid);

        if(!matchcard || !matchcard.inPlay){

            console.error("notify card stats can't find target card");
            return;
        }

        // Check what changed and create floating damage indicators //
        let defencechanged = defence - matchcard.defence;

        let pos = matchcard.getRect().getCenter();
        
        if(defencechanged < 0){

            
            this.rules.floatingNumbers.push(new FloatingNumber(
                ""+defencechanged, new Rect(pos.x, pos.y, 0, 32), 1250, -45, "red"));

        } else if(defencechanged > 0){

            this.rules.floatingNumbers.push(new FloatingNumber(
                ""+defencechanged, new Rect(pos.x, pos.y, 0, 32), 1250, -45, "green"));
        } else {
            
            // No damage //
            // Only show if attack hasn't changed
            if(matchcard.attack == attack){

                this.rules.floatingNumbers.push(new FloatingNumber(
                    ""+defencechanged, new Rect(pos.x, pos.y, 0, 32), 1250, -45, "blue"));
            }
        }
        
        // Update properties //
        matchcard.attack = attack;
        matchcard.defence = defence;
    }

    notifyPlaySpellEffect(effectName: string, targetCard: number, source: string){

        // Find target //
        let matchcard = this.rules.getCardByMatchID(targetCard);

        if(!matchcard){

            console.error("couldn't find target for spell effect");
            return;
        }
        
        let center;

        if(source == "center"){

            center = config.center();
            
        } else {

            // Source is forum //
            center = this.forum.getPixelCenter();
        }

        let x = center.x;
        let y = center.y;

        let effect = resourcemanager.getSpellEffectImage(effectName);

        if(effect == null){

            console.error("Invalid effect name: " + effectName);
            return;
        }

        let cardmiddle = matchcard.getRect().getCenter();
        
        this.rules.addNewEffect(new SpellEffect(
            effect.image, 350, new Rect(x, y, effect.width, effect.height),
            new Pos2(cardmiddle.x, cardmiddle.y)));
    }

    notifyGameRuleSettings(values: any){

        // Updates objective displays etc.
        if(values["maxwinnerratings"] !== undefined)
            config.ratingsToWin = values["maxwinnerratings"];
        
        
    }

    // Returns the player back to the main menu
    notifyExitMatch(){

        console.log("Exiting match");

        this.opponent = "";
        
        this.setGameState(GAME_STATE.Main);
    }

    // Stops the match and shows a score message
    notifyMatchWon(resultstr: string, clientwon: boolean, clientscore: number){

        this.rules.onMatchWon();

        this.matchScoresWindow = new WinScreen(resultstr, clientwon, clientscore);
    }

    /**
       Called when the server requests us to pick a target
    */
    startSelectTargetMode(message: string,
                          // Target definition
                          targetscard: boolean, targetsown: boolean,
                          targetsopponents: boolean)
    {

        this.selectTargetString = message;
        this.selectingTarget = true;

        // Apply other properties only if they are defined, so it's easy to return to this mode
        // if the targeting fails
        if(targetscard !== undefined){

            this.selectTargetsCard = targetscard;
        }
        if(targetsown !== undefined){
            
            this.selectTargetsOwn = targetsown;
        }
        if(targetsopponents !== undefined){

            this.selectTargetsOpponents = targetsopponents;
        }

        // Reset variables //
        this.selectCurrentPosition = null;
        this.selectValidTarget = false;
        this.selectTargetCard = null;
        
    }

    endSelectTargetMode(){

        this.selectingTarget = false;
    }

    //
    // Gamestate modifying functions that aren't directly called from messages //
    //
    removeCardFromHand(matchcard: Card, own: boolean){

        if(own)
            this.ownHand.removeCard(matchcard);
        else
            this.opponentsHand.removeCard(matchcard);
    }

    returnCardToHand(matchcard: Card, own: boolean){

        matchcard.occluded = false;

        if(own)
            this.ownHand.addCard(matchcard);
        else
            this.opponentsHand.addCard(matchcard);
    }

    

    //
    // Mouse input functions //
    //
    injectMouseMove = (e: JQueryMouseEventObject) => {

        if(this.State != GAME_STATE.Match || this.rules.matchShowScores)
            return;

        this.mousePos = new Pos2(e.pageX, e.pageY);

        // Close card popup if no longer over the card //
        if(this.hoveredCard != null){

            // Check is it still hovered
            if(!this.hoveredCard.isPointInside(this.mousePos)){

                this.resetHover();
            }
        }

        // UI with mouse events //
        for(let clickable of this.uiClickable){

            if(clickable.mouseMove(this.mousePos)){

                break;
            }
        }

        // Update mouse pos if selecting a target //
        if(this.selectingTarget)
            this.selectCurrentPosition = this.mousePos;

        // Popup new info if not dragging //
        if(this.hoveredCard == null && !this.isDragging){

            // Check for hovering new cards //
            let hoveredcard = this.getCardAtPoint(this.mousePos);

            if(hoveredcard){

                this.resetHover();
                
                // Set new // 
                this.hoveredCard = hoveredcard;
                this.hoveredCard.hovered = true;
            }
        }

        // drag //
        if(this.mouseLeftDown && !this.selectingTarget){

            if(!this.isDragging){

                // Check should start dragging //
                let moveddistance = this.mouseDownStart.distance(this.mousePos);

                if(moveddistance >= 10 && this.rules.ownTurn){

                    let targetcard = this.getCardAtPoint(this.mousePos);

                    // Only allow dragging visible cards //
                    if(targetcard && !targetcard.occluded && targetcard.own &&
                       !targetcard.banned)
                    {
                        this.isDragging = true;
                        this.draggedCard = targetcard;
                        this.draggedCard.dragged = true;
                    }
                }

            } else {

                if(!this.draggedCard)
                    throw "not good, draggedCard";

                // Continue dragging //
                this.draggedCard.setPos(new Pos2(this.mousePos.x + 1, this.mousePos.y + 1));

                // Check for play card //
                this.dragTarget = DRAG_TARGET.None;
                
                if(!this.draggedCard.inPlay){
                    
                    if(this.mainBattlefield.notifyCardOver(this.mousePos, this.draggedCard)){

                        // Can play the card //
                        this.dragTarget = DRAG_TARGET.PlayCard;
                    }
                    
                } else {

                    // Check attack //
                    if(!this.draggedCard.attackedThisTurn){
                        
                        let targetcard = this.getCardAtPoint(this.mousePos);

                        if(targetcard && !targetcard.own && targetcard.inPlay){

                            this.dragTarget = DRAG_TARGET.AttackCard;
                            this.dragAttackTarget = targetcard;
                        }
                    }
                }
            }
        }

        // Update target card //
        if(this.selectingTarget && this.selectTargetsCard){

            // Check for target //
            let targetcard = this.getCardAtPoint(this.mousePos);

            if(targetcard){

                // Check constraints //
                if((targetcard.own && this.selectTargetsOwn) ||
                   (!targetcard.own && this.selectTargetsOpponents))
                {
                    
                    this.selectValidTarget = true;
                    this.selectTargetCard = targetcard;
                    return;
                }
            }

            this.selectValidTarget = false;
        }
    }

    injectMouseDown = (e: JQueryMouseEventObject) => {

        if(this.State != GAME_STATE.Match)
            return;

        if(this.selectingTarget){

            if(e.button == 0){

                // Select target //
                if(this.selectValidTarget){

                    if(this.selectTargetCard){

                        let action = new SelectTargetAction(true, this.selectTargetCard);

                        this.rules.doClientAction(action);
                    }
                } else {

                    // Check for discard //
                    // TODO: make the top bar for target select an object
                    if(e.pageX < config.width / 3 && e.pageY < config.font(28) + 10){

                        // Discard it //
                        this.networking.send(new DiscardCardEffectMessage());
                        this.endSelectTargetMode();
                    }
                }
            }
            
        } else {

            if(e.button == 0){

                this.mouseLeftDown = true;
                this.mouseDownStart = new Pos2(e.pageX, e.pageY);
            }
        }
        
    }

    injectMouseUp = (e: JQueryMouseEventObject) => {

        if(e.button == 0){
            this.mouseLeftDown = false;
        }

        if(this.State != GAME_STATE.Match || this.rules.matchShowScores)
            return;

        if(e.button == 0){

            if(this.isDragging){

                if(!this.draggedCard)
                    throw "not good, draggedCard";

                // End dragging //

                if(this.dragTarget == DRAG_TARGET.AttackCard){
                    
                    // Do attack //
                    let action = new AttackAction(true, this.draggedCard,
                                                  this.dragAttackTarget, undefined);

                    this.rules.doClientAction(action);
                    
                } else if(this.dragTarget == DRAG_TARGET.PlayCard){

                    // Try to play the card //
                    if(this.rules.canPlayCard(this.draggedCard, this.mainBattlefield)){
                        
                        // Do the action //
                        let playaction = new PlayCardAction(true, this.draggedCard);

                        this.rules.doClientAction(playaction);
                    }
                } 

                this.resetDrag();
                return;
            }

            let pos = new Pos2(e.pageX, e.pageY);
            
            // Check buttons and stuff when not dragging //
            // UI with mouse events //
            for(let clickable of this.uiClickable){

                if(clickable.clickCheck(pos)){

                    return;
                }
            }
        }
    }


    /**
       Called when mouse leaves the play area, resets hover and drag actions
    */
    injectMouseLeft = (e: JQueryMouseEventObject) => {

        if(this.State != GAME_STATE.Match)
            return;

        this.resetDragAndHover();
    }

    //
    // Drag helper functions
    //

    /**
       Resets all dragging related variables and returns the dragged card back to home position
    */
    resetDrag(){

        if(!this.isDragging)
            return;

        this.isDragging = false;
        this.dragTarget = DRAG_TARGET.None;
        
        // Reset battlefield colour //
        this.mainBattlefield.notifyCardOver(null, null);

        if(this.draggedCard != null){

            this.draggedCard.dragged = false;
            this.draggedCard.moveHome(450, false);
            this.draggedCard = null;
        }
    }

    resetHover(){

        if(this.hoveredCard != null){

            this.hoveredCard.hovered = false;
            this.hoveredCard = null;
        }
        
    }

    resetDragAndHover(){

        this.resetHover();
        this.resetDrag();
    }

    // Button callbacks

    pressedEndTurn = () => {

        if(this.rules.ownTurn){

            this.networking.send(new EndTurnRequestMessage());
            this.turnEndButton.enabled = false;
        }
    }

    pressedGiveUp = () => {

        this.networking.send(new ClientRequestDefeatMessage());
        this.giveUpButton.enabled = false;
        // TODO: reset timer if this button didn't work for some reason
    }


    // Random utility stuff at the end //
    
    sortPlayerList(){

        // Rebuild list //
        let list = $("#PlayerList");
        
        // Clear list //
        list.empty();

        for(let element of this.onlinePlayerList){

            if(element.name == this.getPlayerName()){

                // Own entry //
                list.append($('<li>').append($('<span>').addClass("Gray").
                                             text(element.name + " (you)")));
                continue;
            }
            
            if(!element.challenge){

                // In match //
                list.append($('<li>').append($('<span>').addClass("Gray").
                                             text(element.name + " (in match)")));

                continue;
            }

            list.append($('<li>').text(element.name).click(() => {

                console.log("Challenging player: " + element.name);
                this.doRequestMatch(element.name, 60);
                
            }));
        }
        
        (window as any).tinysort("ul#PlayerList>li");
    }

}


export let game = new Game();

