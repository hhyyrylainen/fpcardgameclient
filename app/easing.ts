/**
   Easing functions for animations.

   These are taken from various sources
*/

// Original equations by
/*
 *
 * TERMS OF USE - EASING EQUATIONS
 * 
 * Open source under the BSD License. 
 * 
 * Copyright Â© 2001 Robert Penner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * Redistributions of source code must retain the above copyright notice, this list of 
 * conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list 
 * of conditions and the following disclaimer in the documentation and/or other materials 
 * provided with the distribution.
 * 
 * Neither the name of the author nor the names of contributors may be used to endorse 
 * or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED 
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE. 
 *
 */

// Adapted from
/*
 * jQuery Easing v1.3 - http://gsgd.co.uk/sandbox/jquery/easing/
 *
 * Uses the built in easing capabilities added In jQuery 1.1
 * to offer multiple easing options
 *
 * TERMS OF USE - jQuery Easing
 * 
 * Open source under the BSD License. 
 * 
 * Copyright Â© 2008 George McGinley Smith
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * Redistributions of source code must retain the above copyright notice, this list of 
 * conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list 
 * of conditions and the following disclaimer in the documentation and/or other materials 
 * provided with the distribution.
 * 
 * Neither the name of the author nor the names of contributors may be used to endorse 
 * or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED 
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE. 
 *
 */

// For reference old variable descriptions:
// t: current time, b: begInnIng value, c: change In value, d: duration

export function easeOutBounce(t: number): number {
	if (t < (1/2.75)) {
		return (7.5625*t*t);
	} else if (t < (2/2.75)) {
		return (7.5625*(t-=(1.5/2.75))*t + .75);
	} else if (t < (2.5/2.75)) {
		return (7.5625*(t-=(2.25/2.75))*t + .9375);
	} else {
		return (7.5625*(t-=(2.625/2.75))*t + .984375);
	}
}


// https://gist.github.com/gre/1650294
/*
 * Easing Functions - inspired from http://gizma.com/easing/
 * only considering the t value for the range [0, 1] => [0, 1]
 */
// accelerating from zero velocity
export function easeInQuad(t: number ): number {
 return t*t 
}
// decelerating to zero velocity
export function easeOutQuad(t: number ): number {
 return t*(2-t) 
}
// acceleration until halfway, then deceleration
export function easeInOutQuad(t: number ): number {
 return t<.5 ? 2*t*t : -1+(4-2*t)*t 
}
// accelerating from zero velocity 
export function easeInCubic(t: number ): number {
 return t*t*t 
}
// decelerating to zero velocity 
export function easeOutCubic(t: number ): number {
 return (--t)*t*t+1 
}
// acceleration until halfway, then deceleration 
export function easeInOutCubic(t: number ): number {
 return t<.5 ? 4*t*t*t : (t-1)*(2*t-2)*(2*t-2)+1 
}
// accelerating from zero velocity 
export function easeInQuart(t: number ): number {
 return t*t*t*t 
}
// decelerating to zero velocity 
export function easeOutQuart(t: number ): number {
 return 1-(--t)*t*t*t 
}
// acceleration until halfway, then deceleration
export function easeInOutQuart(t: number ): number {
 return t<.5 ? 8*t*t*t*t : 1-8*(--t)*t*t*t 
}
// accelerating from zero velocity
export function easeInQuint(t: number ): number {
 return t*t*t*t*t 
}
// decelerating to zero velocity
export function easeOutQuint(t: number ): number {
 return 1+(--t)*t*t*t*t 
}
// acceleration until halfway, then deceleration 
export function easeInOutQuint(t: number ): number {
 return t<.5 ? 16*t*t*t*t*t : 1+16*(--t)*t*t*t*t
}

// More by ChristianFigueroa
export function easeInElastic(t: number): number {
    return (.04 - .04 / t) * Math.sin(25 * t) + 1
}
// elastic bounce effect at the end
export function easeOutElastic(t: number): number {
    return .04 * t / (--t) * Math.sin(25 * t)
}
// elastic bounce effect at the beginning and end
export function easeInOutElastic(t: number): number {
    return (t -= .5) < 0 ? (.01 + .01 / t) * Math.sin(50 * t) :
        (.02 - .01 / t) * Math.sin(50 * t) + 1
}

// And a few more by Kerndog73
export function easeInSin(t: number): number {
    return 1 + Math.sin(Math.PI / 2 * t - Math.PI / 2);
}
export function easeOutSin(t: number): number {
    return Math.sin(Math.PI / 2 * t);
}
export function easeInOutSin(t: number): number {
    return (1 + Math.sin(Math.PI * t - Math.PI / 2)) / 2;
}
