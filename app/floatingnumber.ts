
import { Rect } from "./types";
import { Renderable } from "./drawable";
import config from "./config";


export class FloatingNumber extends Renderable{

    protected timePassed: number = 0;

    /**
       possize The x and y pixel positions and the height of the text
    */
    constructor(public text: string, possize: Rect, public aliveMS: number,
                public speed: number, public colour: string)
    {
        super(possize, config.DRAW_ORDER_FLOATING_NUMBER);
    }

    draw(ctx: CanvasRenderingContext2D, delta: number): boolean{

        this.timePassed += delta;

        if(this.timePassed > this.aliveMS)
            return true;

        this.rect.y += this.speed * 0.001 * delta;

        ctx.fillStyle = this.colour;
        ctx.textAlign = "center";
        ctx.font = "bold " + this.rect.height + "px serif";

        ctx.fillText(this.text, this.rect.x, this.rect.y);
        
        return false;
    }
    
}
