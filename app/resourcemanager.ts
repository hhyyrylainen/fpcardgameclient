import * as $ from 'jquery';

import { ImageWithSize } from "./types";

//! Manages downloading all the correct resources before the game can start
class ResourceManager{

    private downloads: ThingToDownload[] = [];

    // Resources //
    // Plain images
    fpBackgroundImg: HTMLImageElement;
    imageBlackGray: HTMLImageElement;

    cardFaceDownImg: HTMLImageElement;

    // Non-changing svgs
    effectFlame1: HTMLImageElement;
    effectHeal: HTMLImageElement;

    // Changing svgs
    normalShield: HTMLImageElement;
    activeSword: HTMLImageElement;
    disabledSword: HTMLImageElement;
    normalEnergy: HTMLImageElement;

    // canvas resources //
    // patterns
    tileBlackGray: CanvasPattern;
    fpBackground: CanvasPattern;

    constructor(){
        
    }

    // Downloads common rendering resources
    dlResources(){

        this.downloads.push(new ImageDownload("/images/ui-bg.png", (obj: ImageDownload) => {

            this.fpBackgroundImg = obj.img;
        }));

        this.downloads.push(new ImageDownload("/images/128-192.jpg", (obj: ImageDownload) => {
            
            this.imageBlackGray = obj.img;
        }));
        
        this.downloads.push(new ImageDownload(
            "/images/effects/flame-1.svg", (obj: ImageDownload) =>
                {
                    this.effectFlame1 = obj.img;
                }));

        this.downloads.push(new ImageDownload(
            "/images/effects/heal.svg", (obj: ImageDownload) =>
                {
                    this.effectHeal = obj.img;
                }));
        
        

        this.downloads.push(new ImageDownload(
            "/images/cards/FaceDown.png", (obj: ImageDownload) =>
                {
                    
                    this.cardFaceDownImg = obj.img;
                }));
        
        // SVG elements //
        this.downloads.push(new jQuerySVGDownload(
            "/images/icons/shield.svg",
            new jQueryTransform((data: string): string => {
                return data.replace(/#FFFFFF/g, '#8a8a5c');
                
            }, (img: HTMLImageElement) => {
                
                this.normalShield = img;
            })));
            


        this.downloads.push(new jQueryMulti(
            "/images/icons/war.svg",
            [
                new jQueryTransform((data: string): string => {
                    
                    return data.replace(/#FFFFFF/g, '#00b374');
                    
                }, (img: HTMLImageElement) => {
                                        
                    this.activeSword = img;
                }),
                new jQueryTransform((data: string): string => {
                    
                    return data.replace(/#FFFFFF/g, '#D9D9D9');
                    
                }, (img: HTMLImageElement) => {
                                        
                    this.disabledSword = img;
                })
            ]));

        this.downloads.push(new jQuerySVGDownload(
            "/images/icons/nature.svg",
            new jQueryTransform((data: string): string => {
                
                return data.replace(/#FFFFFF/g, '#4da6ff');
                
            }, (img: HTMLImageElement) => {
                
                this.normalEnergy = img;
            })));

    }

    //! Returns true when resources have been downloaded
    dlReady(): boolean{

        for(let dl of this.downloads){

            if(!dl.isReady)
                return false;
        }

        return true;
    }

    //! Creates actual resource objects after downloading (some are created while downloading)
    createResources(ctx: CanvasRenderingContext2D){

        if(!this.dlReady())
            throw "createResources called before resources are ready";

        for(let dl of this.downloads){
            
            dl.onFinish();
        }


        this.fpBackground = ctx.createPattern(this.fpBackgroundImg, 'repeat');

        this.tileBlackGray = ctx.createPattern(this.imageBlackGray, 'repeat');
    }

    //! Returns an effect resource
    getSpellEffectImage(name: string): ImageWithSize{

        if(name == "flame"){
            return {
                image: this.effectFlame1,
                width: 131,
                height: 199
            };
        }

        if(name == "heal"){
            return {
                image: this.effectHeal,
                width: 120,
                height: 120
            };
        }

        return null;
    }
}

abstract class ThingToDownload{

    abstract get isReady(): boolean;

    //! Called when all resources have been downloaded
    abstract onFinish(): void;
}

class ImageDownload extends ThingToDownload{

    img: HTMLImageElement;

    constructor(src: string, private finish: (obj: ImageDownload) => void){

        super();
        this.img = new Image();
        this.img.src = src;
    }

    onFinish(): void{

        this.finish(this);
    }
    
    get isReady(): boolean{

        return this.img.complete;
    }
}

/**
   Receives data from jQuery and does stuff with it
*/
class jQueryTransform{
    
    constructor(
        /**
           Applied to data after receiving
        */
        public transform: (data: string) => string,
        /**
           Called to store the resource
        */
        public finish: (img: HTMLImageElement) => void)
    {
        
    }
}

class jQuerySVGDownload extends ThingToDownload{

    img: HTMLImageElement;
    ready: boolean = false;

    constructor(src: string, private dataHandler: jQueryTransform)
    {
        super();
        this.img = new Image();

        $.get(src, (data: any) => {
            
            let DOMURL = window.URL;

            if(!DOMURL){

                throw "Browser not supported, because DOMURL (window.URL) is missing";
            }

            let encodedsvg = new Blob([dataHandler.transform(<string>data)],
                                      {type: 'image/svg+xml;charset=utf-8'});
            
            let url = DOMURL.createObjectURL(encodedsvg);

            this.img.onload = function () {

                DOMURL.revokeObjectURL(url);
            }

            this.img.src = url;
            this.ready = true;
            
        }, "text");
    }

    onFinish(): void{

        this.dataHandler.finish(this.img);
    }
    
    get isReady(): boolean{

        return this.ready;
    }
}

/**
   Downloads a file once for multiple resources
*/
class jQueryMulti extends ThingToDownload{

    imgs: Array<HTMLImageElement> = [];
    ready: boolean = false;

    constructor(src: string, private dataHandler: Array<jQueryTransform>)
    {
        super();

        $.get(src, (data: any) => {
            
            let DOMURL = window.URL;

            if(!DOMURL){

                throw "Browser not supported, because DOMURL (window.URL) is missing";
            }

            for(let handler of dataHandler){

                let img = new Image();
                
                this.imgs.push(img);

                let encodedsvg = new Blob([handler.transform(<string>data)],
                                          {type: 'image/svg+xml;charset=utf-8'});
                
                let url = DOMURL.createObjectURL(encodedsvg);

                img.onload = function () {

                    DOMURL.revokeObjectURL(url);
                }

                img.src = url;
            }
            
            this.ready = true;
            
        }, "text");
    }

    onFinish(): void{

        for(let i = 0; i < this.dataHandler.length; ++i){

            this.dataHandler[i].finish(this.imgs[i]);
        }
    }
    
    get isReady(): boolean{

        return this.ready;
    }
}

export default new ResourceManager();


