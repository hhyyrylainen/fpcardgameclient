import { Pos2 } from "./types";
import { FontDef } from "./drawable";

/**
   Text that fits into a rect by splitting lines automatically on word boundaries
*/
export class WrappedText{

    protected isDirty: boolean = true;
    protected lines: Array<string>;

    protected totalLinesHeight: number = 0;

    /**
       Number of pixels between lines
    */
    protected lineSpacing: number = 2;

    /**
       The resulting height of all the lines
    */
    get height(): number{

        if(this.isDirty)
            return null;
        
        return this.totalLinesHeight;
    }

    get text(){

        return this._text;
    }

    set text(val: string){

        if(this._text == val)
            return;

        this.isDirty = true;
        this._text = val;
    }

    set wrapWidth(val: number){

        if(this._wrapWidth == val)
            return;
        
        this.isDirty = true;
        this._wrapWidth = val;
    }

    get wrapWidth(){

        return this._wrapWidth;
    }

    set font(val: FontDef){

        this.isDirty = true;
        this._font = val;
    }

    get font(){

        return this._font;
    }
    
    constructor(private _text: string, private _wrapWidth: number, public pos: Pos2,
                private _font: FontDef)
    {
        
    }

    /**
       Applies the font for use
    */
    applyFont(ctx: CanvasRenderingContext2D){

        if(!this._font)
            throw "drawing wrappedtext without font";

        this._font.fontApply(ctx);
    }

    /**
       Draws the text
    */
    draw(ctx: CanvasRenderingContext2D, delta: number): boolean{

        if(this.isDirty)
            this.calculateLines(ctx);

        this.applyFont(ctx);

        let yOffset = 0;
        
        for(let line of this.lines){
            
            ctx.fillText(line, this.pos.x, this.pos.y + yOffset, this.wrapWidth);

            // Update offset for next line
            yOffset += this._font.fontSize + this.lineSpacing;
        }

        return false;
    }

    /**
       Calculates text wrapping if dirty
    */
    validate(ctx: CanvasRenderingContext2D){

        if(this.isDirty)
            this.calculateLines(ctx);
    }

    /**
       Returns true if dirty
    */
    needsValidation(): boolean{

        return this.isDirty;
    }

    /**
       Calculates the line wrap positions

       Is called before rendering if this is marked dirty

       TODO: allow explicit \n for linebreaks
    */
    calculateLines(ctx: CanvasRenderingContext2D){

        this.lines = [];

        let words = this.text.split(" ");

        let currentLine = "";

        this.applyFont(ctx);
        
        for(let word of words){
            
            // Check can the fit on current line
            let newLineIfFits = currentLine.length > 0 ? currentLine + " " + word :
                word;

            let lineLength = ctx.measureText(newLineIfFits).width;

            if(lineLength >= this.wrapWidth && currentLine.length > 0){
                // Next line
                this.lines.push(currentLine);
                currentLine = word;

            } else {
                
                currentLine = newLineIfFits
            }
        }

        if(currentLine.length > 0)
            this.lines.push(currentLine);

        // Update total height //
        this.totalLinesHeight = this.lines.length * (this._font.fontSize + this.lineSpacing);
        
        this.isDirty = false;
    }
}

