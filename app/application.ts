
// jQuery import
import * as $ from 'jquery';

// Dirty hack to make jqueryui load properly
(window as any).jQuery = $;
import 'jquery-ui-dist/jquery-ui';

import { game } from "./gamemain";

import * as common from "./common";
import resourcemanager from "./resourcemanager";

import { NameStore } from './namestore';


class App{

    storedName = new NameStore();

    startTimeout: number;

    constructor(){


    }

    init(){

        resourcemanager.dlResources();

        $("#LoadingIndicator").progressbar({
            value: false
        });

        $("#StartGameButton").button().click(( event: JQueryEventObject ) => {

            event.preventDefault();
            
            this.tryStartGame();
        });
        
        /*
         * Replace all SVG images with inline SVG
         */
        jQuery('img.svg').each(function(index: number, element: Element){
            var $img = jQuery(element);
            var imgID = $img.attr('id');
            var imgClass = $img.attr('class');
            var imgURL = $img.attr('src');

            jQuery.get(imgURL, function(data: any) {
                // Get the SVG tag, ignore the rest
                var $svg = jQuery(data).find('svg');

                // Add replaced image's ID to the new SVG
                if(typeof imgID !== 'undefined') {
                    $svg = $svg.attr('id', imgID);
                }
                // Add replaced image's classes to the new SVG
                if(typeof imgClass !== 'undefined') {
                    $svg = $svg.attr('class', imgClass+' replaced-svg');
                }

                // Remove any invalid XML tags as per http://validator.w3.org
                $svg = $svg.removeAttr('xmlns:a');

                // Check if the viewport is set,
                // if the viewport is not set the SVG wont't scale.
                if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')){
                    
                    $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' +
                              $svg.attr('width'))
                }

                // Replace image with new SVG
                $img.replaceWith($svg);

            }, 'xml');

        });
        

        let gameBox = $("#GameBox");

        let fixoffsets = function(e: JQueryMouseEventObject){

            let pos = gameBox.offset();
            
            e.pageX = e.pageX - pos.left;
            e.pageY = e.pageY - pos.top;
        }

        gameBox.mousemove(function(e: JQueryMouseEventObject){

            fixoffsets(e);
            game.injectMouseMove(e);
        });

        gameBox.mouseleave(function(e: JQueryMouseEventObject){

            game.injectMouseLeft(e);
        });

        gameBox.mousedown(function(e: JQueryMouseEventObject){

            fixoffsets(e);
            game.injectMouseDown(e);
        });

        gameBox.mouseup(function(e: JQueryMouseEventObject){

            fixoffsets(e);
            game.injectMouseUp(e);
        });


        // Stored name loading //
        let name = this.storedName.loadName();

        if(!name){

            $("#NameBox").val("Mingebag-" + Math.floor(1000 + Math.random() * 9000));
            $("#RememberNameBox").prop('checked', false);
            
        } else {
        
            $("#NameBox").val(name);
            $("#RememberNameBox").prop('checked', true);
        }
        
        console.log("App initialized");

        // Quickstart check //
        if(common.getURLParameter("autostart") == "true"){

            console.log("Autostarting, because ?autostart=='true'");

            this.doAutoStart();
            
        } else if(common.getURLParameter("automatch") == "true"){

            console.log("Autostarting, because ?automatch=='true'");

            this.doAutoStart();
            
            // This will be done once game post init finishes //
            game.playEasyMatch();
        }

        // Error message check //
        var errormessage = common.getURLParameter("errormessage");

        if(errormessage){

            this.showError(errormessage);
        }
    }

    doAutoStart(){

        if(!this.tryStartGame()){

            // Queue start //
            console.log("Couldn't autostart yet, trying again and again");
            this.showError("Autostarting game...");

            
            
            this.startTimeout = window.setTimeout(() => {

                if(this.tryStartGame()){
                    
                    window.clearTimeout(this.startTimeout);
                    
                } else {
                    
                    this.showError("Autostarting game...");
                }
                
            }, 250);
        }
    }
    

    hideError(){

        $("#ErrorContainer").hide();
    }
    
    showError(message: string){

        $("#ErrorContainer").show();
        $("#MainErrorMessage").html(message);
    }

    //! Starts the game or shows an error element
    tryStartGame(): boolean{

        // Error check //
        var value = $("#NameBox").val();

        if(value.length < 1){

            this.showError("Name is too short / reserved");
            return false;
        }

        // Check that resources are ready //
        if(!resourcemanager.dlReady()){

            this.showError("Resources haven't been downloaded. Please wait a few seconds");
            return false;
        }

        console.log("Starting game as " + value);

        // Store player name if wanted //
        if($("#RememberNameBox").is(':checked')){

            this.storedName.storeName(value);
            
        } else {

            this.storedName.clearName();
        }

        game.setPlayerName(value);

        this.hideError();

        // Clear errormessage //
        history.replaceState({}, "FPCardGame", "/index.html");
        
        // Starts the game //
        this.changeToGame();

        return true;
    }

    // Creates a canvas and assigns it, starts game tick function
    changeToGame(){

        // Create canvas //
        $("#GameBox").html("<canvas id='GameRender'></canvas>");
        $("#GameBox").css("border-radius", "0px");
        $("#GameBox").css("border", "0px");
        $("#GameBox").css("padding", "0px");
        $("#GameBox").css("overflow-y", "hidden");
        $("#GameBox").removeClass("row content");
        $("#GameBox").css("width", "100%");
        $("#GameBox").css("height", "100%");


        // Hide/show required things //
        $("#SiteHeader").hide();
        
        // Put in a spinner //
        $("#LoadingIndicator").show();

        try{
            
            game.init();
            
        } catch(e){

            game.doErrorRedirect("Error initializing:" + e);
        }
    }
}

const app = new App

export = app
