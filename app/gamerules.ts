import { game } from "./gamemain";
import { BaseAction } from "./actions";
import { Battlefield } from "./battlefield";
import { Card, cardholder } from "./card";
import { DelayApply } from "./types";
import { SpellEffect } from "./spelleffect";
import { TimedText } from "./timedtext";
import { FloatingNumber } from "./floatingnumber";

/**
   Main class for handling all card moves to the field and attacks, stores actions to reverse
   if server disagrees
*/
export class Rules{

    // Client actions //
    private clientActions: { [key: number] : BaseAction } = {};

    private delayedApply: Array<DelayApply> = [];

    // Game objects //
    
    /**
       All existing cards
    */
    cardsInPlay: Array<Card> = []

    /**
       Graphical effects

       Like flying projectiles and stuff
    */
    activeEffects: Array<SpellEffect> = [];

    /**
       Timed text objects
       Turn change texts and stuff
    */
    timedTexts: Array<TimedText> = [];

    /**
       Floating number objects

       Mainly damage numbers after attacks
    */
    floatingNumbers: Array<FloatingNumber> = [];

    // Bookkeeping variables some of these are locally tracked and some are
    // sent to us by the server

    /**
       True when this local player's turn.
       When false prevents the user from doing anything else than forfeiting
    */
    ownTurn: boolean = false;

    /**
       Turn number. Tracked locally
    */
    turnNumber: number = 0;

    /**
       True when local player has posted
    */
    postedThisTurn: boolean = false;
    

    /**
       True when the game has ended
    */
    matchShowScores: boolean = false;


    

    /**
       Sets up this object for a single game. When next game starts this needs to be
       recreated
    */
    constructor(){


    }


    /**
       Queues an action
    */
    delayAction(obj: DelayApply){

        this.delayedApply.push(obj);
    }

    /**
       Executes queued actions
    */
    executeQueue(){

        this.delayedApply = this.delayedApply.filter((value: DelayApply): boolean => {

            if(!value.canApply())
                return true;

            value.apply();
            return false;
        });
    }

    // Client calls these functions to predict server actions //

    doClientAction(action: BaseAction){

        // Apply the action //
        action.apply();
        
        // Store for confirmation //
        this.clientActions[action.idNumber] = action;

        // Send to server //
        console.log("Sending action to server");

        game.networking.send(action.createMessage());
    }


    /**
       Undos a done action, called when a server disagrees with the client
    */
    undoClientAction(actionNumber: number){

        let action = this.clientActions[actionNumber];

        if(!action){
            
            console.warn("Client got told that an undefined action should be undone");
            return;
        }

        // Do undo //
        action.undo();

        // Remove from actions
        this.clientActions[actionNumber] = undefined;
    }

    // Called when a server agreed with the client
    serverAgreed(actionNumber: number){
        
        // Remove from actions
        this.clientActions[actionNumber] = undefined;        
    }

    //
    // Query functions
    //
    getCardByMatchID(matchid: number): Card{

        for(let element of this.cardsInPlay){

            if(element.matchID == matchid)
                return element;
        }

        return null;
    }

    //
    // Rule checking functions
    //

    /**
       Returns true if client can play the card
    */
    canPlayCard(card: Card, field: Battlefield){

        // Own turn check //
        if(!this.ownTurn)
            return false;
        
        // Basic checks //
        if(!card || card.inPlay)
            return false;

        // Energy check //
        if(game.getOwnEnergy() < card.cost)
            return false;
        
        // TODO: rules
        return true;
    }

    /**
       Returns true if a card can be pointed at
    */
    canPointAtCard(card: Card): boolean{

        // Cannot target hidden or voided cards
        if(card.occluded || card.voided)
            return false;
        
        return true;
    }
    
    //
    // Utility and rule keeping stuff
    //
    cardEnterPlay(card: Card, field: Battlefield){

        card.banned = false;
        card.inPlay = true;
        card.dragged = false;
        card.hovered = false;
    }

    cardBanned(card: Card, field: Battlefield){

        card.banned = true;
    }

    undoCardEnterPlay(card: Card, field: Battlefield){

        
        card.inPlay = false;
    }

    onRevealedCard(jsonMSGBlob: any){

        // Find the card //
        for(let card of this.cardsInPlay){
            
            if(card.matchID == jsonMSGBlob["matchcard"]){

                card.reveal(jsonMSGBlob, cardholder);
                return;
            }
        }
    }

    addNewEffect(effectObj: SpellEffect){

        this.activeEffects.push(effectObj);
    }

    /**
       Called when turn has changed
    */
    onTurnChanged(own: boolean){

        ++this.turnNumber;

        if(own){
            
            this.ownTurn = true;
            this.postedThisTurn = false;
            
        } else {
            
            this.ownTurn = false;
        }


    }

    /**
       Called when match has ended, all actions should be prevented after this call
    */
    onMatchWon(){

        this.matchShowScores = true;
    }


    //
    // Rendering game objects
    //

    drawStateEarly(ctx: CanvasRenderingContext2D, delta: number){
        
        ctx.save();

        for(let card of this.cardsInPlay){

            let res = card.draw(ctx, delta);
        }

        ctx.restore();
    }
    

    drawStateLate(ctx: CanvasRenderingContext2D, delta: number){

        ctx.save();

        // Draw floating numbers //
        for(let i = 0; i < this.floatingNumbers.length; ){

            if(this.floatingNumbers[i].draw(ctx, delta)){

                // Done with this object //
                this.floatingNumbers.splice(i, 1);
                
            } else {

                ++i;
            }
        }

        ctx.restore();

        ctx.save();
        
        // Draw effects //
        for(let i = 0; i < this.activeEffects.length; ){

            if(this.activeEffects[i].draw(ctx, delta)){

                // Done with this object //
                this.activeEffects.splice(i, 1);
                
            } else {

                ++i;
            }
        }
        
        ctx.restore();

        // Card info pop-ups
        ctx.save();
        
        for(let card of this.cardsInPlay){

            card.drawPopup(ctx, delta);
        }
        
        ctx.restore();
        

        ctx.save();

        // Timed texts //
        for(let i = 0; i < this.timedTexts.length; ){

            if(this.timedTexts[i].draw(ctx, delta)){

                // Done with this object //
                this.timedTexts.splice(i, 1);
                
            } else {

                ++i;
            }
        }

        ctx.restore();
    }
    
}







