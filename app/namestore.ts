
export class NameStore{

    static KEY_NAME: string = "fp-stored-ply-name";
    
    loadName(): string{

        if(!this.canUse())
            return null;

        return window.localStorage.getItem(NameStore.KEY_NAME);
    }

    storeName(name: string): boolean{

        if(!this.canUse())
            return false;

        try{
            window.localStorage.setItem(NameStore.KEY_NAME, name);
        } catch( error ){
            return false;
        }
        
        return true;
    }
        
    //! Removes name from storage
    clearName(): boolean{
        
        if(!this.canUse())
            return false;

        window.localStorage.removeItem(NameStore.KEY_NAME);
        return true;
    }
    
    canUse(): boolean{

        return window.localStorage ? true : false;
    }
}
