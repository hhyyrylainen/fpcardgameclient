// Base gameplay actions that are verified by the server
// and can be then reverted by the client if they weren't successfull

import { BaseMessage, ActionPlayCard, ActionAttack, ActionDoPost, ActionSelectTarget
       } from "./messages";
import { Choice } from "./forum";
import { Card, CardType, CardAnimationAttact } from "./card";
import { game } from "./gamemain";

// Client's actions are identified by a sequence number
let actionNumber = 1;


export abstract class BaseAction{

    gameAction = true;
    idNumber = ++actionNumber;

    constructor(
        /**
           True if done by local player
        */
        public ownAction: boolean)
    {

    }

    /**
       Applies the effect of this action. Should throw if fails
    */
    abstract apply(): void;

    /**
       Undos this action
    */
    abstract undo(): void;

    /**
       Returns true if this is a valid action to undo (is local player's action)

       otherwise throws an error
    */
    checkUndo(): boolean {

        if(!this.ownAction)
            throw "Action cannot undo opponent's action" 
        return true;
    }

    /**
       Creates a mesage object to tell the server about this action. Should only be attempted
       on own actions
    */
    abstract createMessage(): BaseMessage;

    /**
       Throws an error if this isn't own action
    */
    throwIfNotOwn(): void{

        if(!this.ownAction){

            throw "Tried to apply an action that isn't ours and shouldn't go throug an action";
        }
    }
}

export class PlayCardAction extends BaseAction{

    constructor(ownAction: boolean, private affectedCard: Card) {
        super(ownAction);
    }
    
    apply(){

        // This action needs to be delayed from the opponent
        if(!this.canApply()){

            if(this.ownAction)
                throw "PlayCard own should never be delayed";

            game.rules.delayAction(this);
            return;
        }
        
        game.removeCardFromHand(this.affectedCard, this.ownAction);

        // Don't play the card if it is a spell
        if(this.affectedCard.typeInfo.cardType == CardType.User){
            
            game.mainBattlefield.addCard(this.ownAction, this.affectedCard);
            
        } else {

            console.log("TODO: client predict voiding");
        }
    }

    canApply(): boolean{

        return this.affectedCard.typeInfo != null;
    }
    
    undo(){

        this.checkUndo();
        
        game.mainBattlefield.undoPlayCard(this.affectedCard);

        game.returnCardToHand(this.affectedCard, this.ownAction);
    }

    createMessage(): BaseMessage {

        return new ActionPlayCard(this.idNumber, this.affectedCard.matchID,
                                this.affectedCard.typeID);
    }
}

export class AttackAction extends BaseAction{

    constructor(ownAction: boolean, private attackerCard: Card, private targetCard: Card,
                /**
                   Clientside damage guess
                */
                private damage: number)
    {
        super(ownAction);
    }
    
    apply(){
        
        // Clientside damage calculation
        // TODO: when receiving this action through a message don't make a guess
        if(!this.damage)
            this.damage = 0;

        this.attackerCard.dragged = false;
        this.attackerCard.attackedThisTurn = true;
        this.attackerCard.setToHome();

        // All cards are aligned with top left edge so we want to target the top left
        // Attack current position and not the home position
        let targetPos = this.targetCard.getRect(false).getPos2();
        
        // Play attack animation
        this.attackerCard.setAnimation(new CardAnimationAttact(targetPos, 300, 170));
    }
    
    undo(){
        
        this.checkUndo();
        
        this.attackerCard.attackedThisTurn = false;
        this.attackerCard.moveHome(100, true);
    }
    
    createMessage(): BaseMessage{

        return new ActionAttack(this.idNumber, this.attackerCard.matchID,
                                this.targetCard.matchID);
    }
}

export class DoPostAction extends BaseAction{

    constructor(ownAction: boolean, private choice: Choice){
        super(ownAction);
    }
    
    apply(){

        this.throwIfNotOwn();
        
        game.forum.decayPost(this.choice.id);
        game.rules.postedThisTurn = true;
        
        // This is purely cosmetic
        this.choice.isHighlighted = false;
    }
    undo(){
        
        game.forum.returnFailedPost(this.choice);
        game.rules.postedThisTurn = false;
    }
    
    createMessage(): BaseMessage{

        return new ActionDoPost(this.idNumber, this.choice.id);
    }
}

export class SelectTargetAction extends BaseAction{

    /**
       Prompt text shown to user
    */
    public text = "Undefined Effect";
    
    constructor(ownAction: boolean, private card: Card){
        super(ownAction);
    }
    
    apply(){
        
        this.throwIfNotOwn();
        
        // Just disable select mode, but remember the text
        this.text = game.selectTargetString;
        game.endSelectTargetMode();
    }
    
    undo(){

        // Re-enter targeting mode
        // with the same settings as before
        // The server will (if it works correctly) send a new message prompting for a target
        game.startSelectTargetMode(this.text, game.selectTargetsCard,
                                   game.selectTargetsOwn, game.selectTargetsOpponents);
    }
    
    createMessage(): BaseMessage{

        return new ActionSelectTarget(this.idNumber, "card", this.card.matchID);
    }
}
